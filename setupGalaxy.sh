#!/bin/bash

git clone https://github.com/galaxyproject/galaxy.git

#switch of santitaze javascript
cp ./SyNDITools/galaxy.ini ./galaxy/config/
#include our tools
cp ./SyNDITools/tool_conf.xml ./galaxy/config/

#install biopython
#call the help this will initialize the virtual envirements :)
./galaxy/run.sh --help

source ./galaxy/.venv/bin/activate
pip install biopython
deactivate

#directory change source from http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

#create symbolic link
ln -s $DIR/SyNDITools/ $DIR/galaxy/tools/SyNDITools

(cd $DIR/SyNDITools/Meme2Fimo && tar -xzf meme_4.9.0_4.tar.gz)
(cd $DIR/SyNDITools/Meme2Fimo/meme_4.9.0 && ./configure --prefix=$DIR/SyNDITools/Meme2Fimo/meme --with-url=http://meme-suite.org --enable-build-libxml2 --enable-build-libxslt)
(cd $DIR/SyNDITools/Meme2Fimo/meme_4.9.0 && make)
(cd $DIR/SyNDITools/Meme2Fimo/meme_4.9.0 && sudo make install)

echo "start galaxy by entering the galaxy dir and call run.sh"
echo "do not forget to run install_deps.sh"
