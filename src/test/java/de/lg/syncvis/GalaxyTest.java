package de.lg.syncvis;

/*
import java.util.HashMap;
import java.util.Map;

import com.github.jmchilton.blend4j.galaxy.GalaxyInstance;
import com.github.jmchilton.blend4j.galaxy.GalaxyInstanceFactory;
import com.github.jmchilton.blend4j.galaxy.WorkflowsClient;
import com.github.jmchilton.blend4j.galaxy.beans.WorkflowDetails;
import com.github.jmchilton.blend4j.galaxy.beans.WorkflowInputDefinition;
import com.github.jmchilton.blend4j.galaxy.beans.WorkflowStepDefinition;
import com.github.jmchilton.blend4j.galaxy.ToolsClient;
import com.github.jmchilton.blend4j.galaxy.beans.Tool;
*/
import org.junit.Test;

/**
 * 
 * I think this class is obsolete, it may be worthwhile to remove sometime in
 * the future.
 * 
 * @last update: Jul 22, 2019
 */
public class GalaxyTest {
	  
  /*
   * It seems the following method would require that a Galaxy server is 
   * running, so I think it is better ignore it.
   */

  @Test
  public void test2Blend4j() throws Exception {
  	/*
  	 * Include here a possible code snippet for testing a connection to
  	 * Blend4j. 
  	 */
  }
	
}
