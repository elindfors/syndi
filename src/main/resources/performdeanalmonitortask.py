import sys
from bioblend.galaxy import GalaxyInstance
from bioblend.galaxy.histories import HistoryClient
from bioblend.galaxy.tools import ToolClient
from bioblend.galaxy.datasets import DatasetClient

# This tool is much under development.
# From what is understand the basic idea is to that this tool make a statistical
# test between two samples (read from files).
# Eventually these sample files should be created in Java and this script should
# be called from Java.

GALAXY_URL = '%1$s'
API_KEY = '%2$s'
# TOOL_CATEG_ID = 'my_tools'
# TOOL_ID = 'PlotGenes_R'
TOOL_CATEG_ID = 'Statistics'
TOOL_ID = 't_test_two_samples'

galaxyInstance = GalaxyInstance(url=GALAXY_URL, key=API_KEY)
historyClient = HistoryClient(galaxyInstance)
toolClient = ToolClient(galaxyInstance)
datasetClient = DatasetClient(galaxyInstance)
history = historyClient.create_history('tmp')
import os

firstSampleFilePath = os.getcwd() + '/firstSamples.txt'
firstSampleFile = toolClient.upload_file(firstSampleFilePath, history['id'], type='txt')
secondSampleFilePath = os.getcwd() + '/secondSamples.txt'
secondSampleFile = toolClient.upload_file(secondSampleFilePath, history['id'], type='txt')
params = {'inputFile1':{'src': 'hda', 'id': firstSampleFile['outputs'][0]['id']},'inputFile2':{'src': 'hda', 'id': secondSampleFile['outputs'][0]['id']}} 
output = toolClient.run_tool(history_id=history['id'], tool_id=TOOL_ID, tool_inputs=params)
DATA_SET_ID = output['outputs'][0]['id']
outputId = datasetClient.show_dataset(DATA_SET_ID, hda_ldda='hda')['id']
outputUrl = GALAXY_URL + '/datasets/' + outputId + '/'

print outputUrl
