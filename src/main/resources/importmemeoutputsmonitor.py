GALAXY_URL = '%1$s'
API_KEY = '%2$s'

from bioblend.galaxy import GalaxyInstance
from bioblend.galaxy.histories import HistoryClient
from bioblend.galaxy.datasets import DatasetClient

galaxyInstance = GalaxyInstance(url=GALAXY_URL, key=API_KEY)
datasetClient = DatasetClient(galaxyInstance)
historyClient = HistoryClient(galaxyInstance)
hl = galaxyInstance.histories.get_histories()
noMemeOutputDataSets = 0
if len(hl) > 0:
    for hist in hl:
        dataSetIds = galaxyInstance.histories.show_history(hist['id'], contents=False)['state_ids']['ok']
        for dataSetId in dataSetIds:
            datasetName = datasetClient.show_dataset(dataSetId)['name']
            updateTime = datasetClient.show_dataset(dataSetId)['update_time']
            if ('MEME' in datasetName) and ('xml' in datasetName):
                noMemeOutputDataSets = noMemeOutputDataSets + 1
                print updateTime + ',' + datasetName + ': ' + GALAXY_URL + '/datasets/' + dataSetId
                # print datasetName+': ' + GALAXY_URL + '/datasets/' + dataSetId
if noMemeOutputDataSets == 0:
    print 'NO_MEME_OUTPUT'