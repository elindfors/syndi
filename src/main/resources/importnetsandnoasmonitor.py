GALAXY_URL = '%1$s'
API_KEY = '%2$s'
from bioblend.galaxy import GalaxyInstance
from bioblend.galaxy.histories import HistoryClient
from bioblend.galaxy.datasets import DatasetClient

galaxyInstance = GalaxyInstance(url=GALAXY_URL, key=API_KEY)
datasetClient = DatasetClient(galaxyInstance)
historyClient = HistoryClient(galaxyInstance)
hl = galaxyInstance.histories.get_histories()
containsHist = 'no'
if len(hl) > 0:
    for hist in hl:
        dataSetIds = galaxyInstance.histories.show_history(hist['id'], contents=False)['state_ids']['ok']
        for dataSetId in dataSetIds:
            datasetName = datasetClient.show_dataset(dataSetId)['name']
            if ('.noa' in datasetName) or ('.xgmml' in datasetName):
                print datasetName + ': ' + GALAXY_URL + '/datasets/'+ dataSetId
                containsHist = 'yes'
if containsHist == 'no':
    print 'Galaxy server does not contain any network (.xgmml) and not any node attribute (.noa).'
