import sys
from bioblend.galaxy import GalaxyInstance
from bioblend.galaxy.histories import HistoryClient
from bioblend.galaxy.tools import ToolClient
from bioblend.galaxy.datasets import DatasetClient

GALAXY_URL = '%1$s'
API_KEY = '%2$s'
TOOL_CATEG_ID = 'my_tools'
TOOL_ID = 'PlotGenes_R'

galaxyInstance = GalaxyInstance(url=GALAXY_URL, key=API_KEY)
historyClient = HistoryClient(galaxyInstance)
toolClient = ToolClient(galaxyInstance)
datasetClient = DatasetClient(galaxyInstance)
history = historyClient.create_history('tmp')
import os
genesPath = os.getcwd() + '/tempInputGenes.txt'
geneFile = toolClient.upload_file(genesPath, history['id'], type='txt')
exprPath = os.getcwd() + '/tempInputExpr.txt'
exprFile = toolClient.upload_file(exprPath, history['id'], type='txt')
# %3$s
# params = {'input_genes':{'src': 'hda', 'id': geneFile['outputs'][0]['id']},'input_expr':{'src': 'hda', 'id': exprFile['outputs'][0]['id']},
# 'use_clust|choice':%4$s %5$s }
params = {'genes':{'src': 'hda', 'id': geneFile['outputs'][0]['id']},'ge_expr':{'src': 'hda', 'id': exprFile['outputs'][0]['id']},
'use_clust|choice':'%3$s' '%4$s' }
# 'use_clust|choice':%4$s %5$s }
output = toolClient.run_tool(history_id=history['id'], tool_id=TOOL_ID, tool_inputs=params)
DATA_SET_ID = output['outputs'][0]['id']
outputId = datasetClient.show_dataset(DATA_SET_ID, hda_ldda='hda')['id']
# outputUrl = '" + currentGalUrl + "/datasets/' + outputId + '/'
# outputUrl = '" + GALAXY_URL + "/datasets/' + outputId + '/'
outputUrl = GALAXY_URL + '/datasets/' + outputId + '/'

print outputUrl