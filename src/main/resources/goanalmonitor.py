import sys
from bioblend.galaxy import GalaxyInstance
from bioblend.galaxy.histories import HistoryClient
from bioblend.galaxy.tools import ToolClient
from bioblend.galaxy.datasets import DatasetClient

GALAXY_URL = '%1$s'
API_KEY = '%2$s'

TOOL_CATEG_ID = 'my_tools'
TOOL_ID = 'GoAnalysis_R'

galaxyInstance = GalaxyInstance(url=GALAXY_URL, key=API_KEY)
historyClient = HistoryClient(galaxyInstance)
toolClient = ToolClient(galaxyInstance)
datasetClient = DatasetClient(galaxyInstance)
history = historyClient.create_history('tmp')
import os
genesPath = os.getcwd() + '/tempInputGenes.txt'
geneFile = toolClient.upload_file(genesPath, history['id'], type='txt')
goDirectChildGenesFile = toolClient.upload_file(os.getcwd() + '/tempGoDirectChildGenes.txt', history['id'], type='txt')
goOffspringGenesFile = toolClient.upload_file(os.getcwd() + '/tempGoOffspringGenes.txt', history['id'], type='txt')

# params = {'input_genes':{'src': 'hda', 'id': geneFile['outputs'][0]['id']},'in_go_child_genes':{'src': 'hda', 'id': goDirectChildGenesFile['outputs'][0]['id']},'in_go_offspr_genes':{'src': 'hda', 'id': goOffspringGenesFile['outputs'][0]['id']},'input_go_cat':%3$s}
params = {'genes':{'src': 'hda', 'id': geneFile['outputs'][0]['id']},'gene_to_go_mappings':{'src': 'hda', 'id': goDirectChildGenesFile['outputs'][0]['id']},'gene_to_go_offspr_assocs':{'src': 'hda', 'id': goOffspringGenesFile['outputs'][0]['id']},'go_cat':'%3$s'}

output = toolClient.run_tool(history_id=history['id'], tool_id=TOOL_ID, tool_inputs=params)
DATA_SET_ID = output['outputs'][0]['id']

outputId = datasetClient.show_dataset(DATA_SET_ID, hda_ldda='hda')['id']
outputUrl = GALAXY_URL + '/datasets/' + outputId + '/'
print outputUrl;