import sys
from bioblend.galaxy import GalaxyInstance
from bioblend.galaxy.histories import HistoryClient
from bioblend.galaxy.tools import ToolClient
from bioblend.galaxy.datasets import DatasetClient

GALAXY_URL = '%1$s'
API_KEY = '%2$s'

TOOL_CATEG_ID = 'my_tools'
# TOOL_ID = 'GrapicalGo_R'
TOOL_ID = 'GraphicalGo_R'

galaxyInstance = GalaxyInstance(url=GALAXY_URL, key=API_KEY)
historyClient = HistoryClient(galaxyInstance)
toolClient = ToolClient(galaxyInstance)
datasetClient = DatasetClient(galaxyInstance)
history = historyClient.create_history('tmp')
import os
genesPath = os.getcwd() + '/tempInputGenes.txt'
geneFile = toolClient.upload_file(genesPath, history['id'], type='txt')
graphGeneAndGoFile = toolClient.upload_file(os.getcwd() + '/tempGraphGeneAndGo.txt', history['id'], type='txt')
graphGoAllAnnotGenesFile = toolClient.upload_file(os.getcwd() + '/tempGraphGoAllAnnotGenes.txt', history['id'], type='txt')

# params = {'input_genes':{'src': 'hda', 'id': geneFile['outputs'][0]['id']},'input_gene_gos':{'src': 'hda', 'id': graphGeneAndGoFile['outputs'][0]['id']},'input_all_ann_genes':{'src': 'hda', 'id': graphGoAllAnnotGenesFile['outputs'][0]['id']},'input_go_cat':%3$s,'input_first_sig_nodes':'%4$s'}
params = {'genes':{'src': 'hda', 'id': geneFile['outputs'][0]['id']},'gene_to_go_mappings':{'src': 'hda', 'id': graphGeneAndGoFile['outputs'][0]['id']},'all_ann_genes':{'src': 'hda', 'id': graphGoAllAnnotGenesFile['outputs'][0]['id']},'go_cat':%3$s,'no_top_scor_go_terms':'%4$s'}

output = toolClient.run_tool(history_id=history['id'], tool_id=TOOL_ID, tool_inputs=params)
DATA_SET_ID = output['outputs'][0]['id']

outputId = datasetClient.show_dataset(DATA_SET_ID, hda_ldda='hda')['id']
# outputUrl = GALAXY_URL + '/datasets/' + outputId + '/'
outputUrl = GALAXY_URL + '/datasets/' + outputId + '/display?to_ext=tabular'

# http://localhost:8080/datasets/a0c15f4d91084599/display?to_ext=tabular

print outputUrl