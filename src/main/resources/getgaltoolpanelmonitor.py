# This script is much under development.
# I think the idea is automatically parse relevant statistical
# tools for performdeanalmonitortask.py from Galaxy.

import sys
from bioblend.galaxy import GalaxyInstance
from bioblend.galaxy.tools import ToolClient
GALAXY_URL = '%1$s'
API_KEY = '%2$s'
galaxyInstance = GalaxyInstance(url=GALAXY_URL, key=API_KEY)
toolClient = ToolClient(galaxyInstance)
toolPanels = toolClient.get_tool_panel()
print toolPanels
