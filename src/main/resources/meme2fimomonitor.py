import sys
from bioblend.galaxy import GalaxyInstance
from bioblend.galaxy.histories import HistoryClient
from bioblend.galaxy.tools import ToolClient
from bioblend.galaxy.datasets import DatasetClient

GALAXY_URL = '%1$s'
API_KEY = '%2$s'

# TOOL_CATEG_ID = 'motif_tools'
TOOL_CATEG_ID = 'my_tools'
TOOL_ID = 'Meme2Fimo_rpy'

galaxyInstance = GalaxyInstance(url=GALAXY_URL, key=API_KEY)
historyClient = HistoryClient(galaxyInstance)
toolClient = ToolClient(galaxyInstance)
datasetClient = DatasetClient(galaxyInstance)
history = historyClient.create_history('tmp')
import os

geneFile = toolClient.upload_file(
  os.getcwd() + '/tempMeme2FimoGenes.txt',
  history['id'],
  type='txt'
)
genomeAnnotationFile = toolClient.upload_file(
  os.getcwd() + '/tempMeme2FimoGenomeAnnotation.fasta',
  history['id'],
  type='fasta'
)
params = {
  'genelist':{'src': 'hda', 'id': geneFile['outputs'][0]['id']},
  'genomeAnnotation':{'src': 'hda', 'id': genomeAnnotationFile['outputs'][0]['id']},
  'non_commercial_use':'true'
}

output = toolClient.run_tool(history_id=history['id'], tool_id=TOOL_ID, tool_inputs=params)
DATA_SET_ID = output['outputs'][0]['id']

outputId = datasetClient.show_dataset(DATA_SET_ID, hda_ldda='hda')['id']
outputUrl = GALAXY_URL + '/datasets/' + outputId + '/'

print outputUrl

