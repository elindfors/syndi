import sys
from bioblend.galaxy import GalaxyInstance
from bioblend.galaxy.histories import HistoryClient
from bioblend.galaxy.tools import ToolClient
from bioblend.galaxy.datasets import DatasetClient

GALAXY_URL = '%1$s'
API_KEY = '%2$s'


TOOL_CATEG_ID = 'my_tools'
TOOL_ID = 'Fimo_rpy'

# TOOL_CATEG_ID = 'motif_tools'
# TOOL_ID = 'meme_fimo'

galaxyInstance = GalaxyInstance(url=GALAXY_URL, key=API_KEY)
historyClient = HistoryClient(galaxyInstance)
toolClient = ToolClient(galaxyInstance)
datasetClient = DatasetClient(galaxyInstance)
history = historyClient.create_history('tmp')
import os

geneFile = toolClient.upload_file(
  os.getcwd() + '/tempFimoGenes.txt',
  history['id'],
  type='txt'
)

# seqPath = os.getcwd() + '/tempFimoInputSeq.fasta'
# seqFile = toolClient.upload_file(seqPath, history['id'], type='fasta')

genomeAnnotFile = toolClient.upload_file(
  os.getcwd() + '/tempFimoGenomeAnnotation.fasta',
  history['id'],
  type='fasta'
)

# params = {'inputMotifs':{'src': 'hda', 'id': '%3$s'},'fasta_type|input_database':{'src': 'hda', 'id': seqFile['outputs'][0]['id']},'non_commercial_use':'true'}
params = {
  'genelist':{'src': 'hda', 'id': geneFile['outputs'][0]['id']},
  'genomeAnnotation':{'src': 'hda', 'id': genomeAnnotFile['outputs'][0]['id']},
  'inputMotifs':{'src': 'hda', 'id': '%3$s'},
  # 'galaxyurl':GALAXY_URL,
  'galaxyurl':'%1$s',
  # 'apikey':API_KEY,
  'apikey':'%2$s',
  'non_commercial_use':'true'
}
output = toolClient.run_tool(history_id=history['id'], tool_id=TOOL_ID, tool_inputs=params)
DATA_SET_ID = output['outputs'][0]['id']
outputId = datasetClient.show_dataset(DATA_SET_ID, hda_ldda='hda')['id']
outputUrl = GALAXY_URL + '/datasets/' + outputId + '/'
print outputUrl;