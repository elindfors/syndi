package de.lg.syncvis.internal;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.awt.Component;

import javax.swing.BoxLayout;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JPanel;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

/**
 * This class is adopted from
 * https://github.com/cytoscape/cytoscape-samples/blob
 * /master/sample-use-task-monitor
 * /src/main/java/org/cytoscape/sample/internal/UseTaskMonitorTask.java
 * 
 * @last update: Mar 20, 2018
 */
public class ImportMemeOutputsMonitorTask extends AbstractTask {
	
	/*
	 * We use excepOccured and memeOutputStrings to store a successful Galaxy response
	 * and error message, respectively.
	 * 
	 * In the end (=finalize method), we show a message in a separate pop-up
	 * window based on the above mentioned variables.
	 * 
	 * OBS!
	 * I think we should not use JOptionPane.showMessageDialog for this purpose
	 * since it seems to cause a conflict with taskMonitor, the taskMonitor will
	 * be handing forever. 
	 */
	static Logger logger = LogManager.getLogger(
		ImportMemeOutputsMonitorTask.class.getName()
	);
	MyGalCytoPanel mainApp;
	ButtonGroup memeOutputButtonGroup;
	JPanel memeOutputPanel;
	JPanel fimoPanel;
	JFrame fimoFrame;
	
	public ImportMemeOutputsMonitorTask(
			MyGalCytoPanel mainApp,
			ButtonGroup memeOutputButtonGroup,
			JPanel memeOutputPanel,
			JFrame fimoFrame
	) {
		this.mainApp = mainApp;
		this.memeOutputButtonGroup = memeOutputButtonGroup;
		this.memeOutputPanel = memeOutputPanel;
		this.fimoFrame = fimoFrame;
	}
	
	@Override
	public void run(final TaskMonitor taskMonitor) {
		taskMonitor.setTitle("Importing MEME Output Files from Galaxy ...");
		
		/*
		 * It seems in this method we should not give pop-up messages since it
		 * seems they would lead to infinite loops.
		 * 
		 * We therefore should not use pop-up windows for debugging but it is ok
		 * to print debugs directly to log files.
		 * 
		 * Also, we cannot give any pop-up messages about missing input
		 * parameters.
		 */
		
		/*
		 * It seems taskMonitor.setProgress(0.1) would freeze the progress bar
		 * which would look like an error occurred, so it is probably good not to
		 * use it.
		 */
		// taskMonitor.setProgress(0.1);
		
		
		/*
		 * It seems taskMonitor.setProgress(0.1) would freeze the progress bar
		 * which would look like an error occurred, so it is probably good not to
		 * use it.
		 */
		// taskMonitor.setProgress(0.1);
		
		List<String> memeOutputStrings = new ArrayList<String>();
		boolean excepOccured = false;
		boolean noMemeOutput = false;
		try {
			/*
			 * Call galaxy by using the input text files.
			 * 
			 * It seems this works now. However it seems the outputUrl is available
			 * much earlier than the Galaxy process is completed, so if the user
			 * clicks the outputUrl immediately (s)he will quite likely get an empty
			 * file.
			 */
			String prg = Util.readFile(
				"importmemeoutputsmonitor.py",
				mainApp.currentGalUrl,
				mainApp.currentGalApiKey
			);
			BufferedWriter out = new BufferedWriter(
				new FileWriter("importMemeOutputs.py")
			);
			out.write(prg);
			out.close();
			Process p = Runtime.getRuntime().exec("python importMemeOutputs.py");
			BufferedReader in = new BufferedReader(
				new InputStreamReader(p.getInputStream())
			);
			String currentLine;
			while ((currentLine = in.readLine()) != null) {
				logger.debug("currentLine: " + currentLine);
				if (currentLine.equals("NO_MEME_OUTPUT")) {
					logger.debug("currentLine.equals(NO_MEME_OUTPUT)");
					noMemeOutput = true;
					/*
					logger.debug("currentLine.equals(NO_MEME_OUTPUT) bef JOptionPane.showMessageDialog");
					JOptionPane.showMessageDialog(
						null,
						"Galaxy Server does not contains any result set " +
						"from MEME analysis.\n" +
						"Please run MEME analysis before running FIMO analysis."
					);
					logger.debug("currentLine.equals(NO_MEME_OUTPUT) aft JOptionPane.showMessageDialog");
					*/
					
					break;
				}
				logger.debug("Aft if");
				memeOutputStrings.add(currentLine);
			}
			in.close();
		} catch (Exception e2) {
			excepOccured = true;
			logger.error("e2: " + e2);
		} finally {
			/*
			if (noMemeOutput) {
				JOptionPane.showMessageDialog(
					null,
					"Galaxy Server does not contains any result set " +
					"from MEME analysis.\n" +
					"Please run MEME analysis before running FIMO analysis."
				);
				return;
			}
			*/
			taskMonitor.setProgress(1.0);
			if (excepOccured
					|| memeOutputStrings == null
					|| memeOutputStrings.size() == 0
			) {
				/*
				 * If an error occurred, we open a separate pop-up window in
				 * order to show the error message.
				 */
				JFrame outputFrame = new JFrame();
				outputFrame.setSize(600,720);
				JEditorPane outputPane = new JEditorPane();
				JScrollPane outputScrollPane = new JScrollPane(outputPane);
				outputPane.setEditable(false);
				outputPane.setContentType("text/html");
				String outputHtml = "";
				if (noMemeOutput) {
					outputHtml =
						"<html><font color='red'>" +
						"Galaxy Server does not contains any result set " +
						"from MEME analysis. <br/>" +
						"Please run MEME analysis before running FIMO " +
						"analysis." +
						"</font></html>";
				} else {
					outputHtml =
						"<html><font color='red'>" +
						"An error occured when accessing the Galaxy Server." +
						"<br>" +
						"Possible reasons for the error:" +
						"<ul>" +
						"<li>" +
						"You are not running Cytoscape as administrator." +
						"</li>" +
						"<li>" +
						"'Galaxy Server URL' or 'Galaxy Server API KEY' is wrong." +
						"</li>" +
						"<li>" +
						"Galaxy Server is down." +
						"</li>" +
						"<li>" +
						"Your internet connection is not working." +
						"</li>" +
						"</ul>" +
						"</font></html>";
				}
				outputPane.setText(outputHtml);
				outputFrame.getContentPane().add(outputScrollPane);
				outputFrame.setVisible(true);
				// outputFrame.pack();
			} else {
				/*
				 * If we got a successful response, we parse MEME outputs from
				 * it and show them on radio buttons. 
				 */
				memeOutputPanel.setLayout(new BoxLayout(memeOutputPanel, BoxLayout.Y_AXIS));
				for (int i = 0; i < memeOutputStrings.size(); i++) {
					JRadioButton radioButton = new JRadioButton(memeOutputStrings.get(i));
					radioButton.setToolTipText(memeOutputStrings.get(i));
					memeOutputButtonGroup.add(radioButton);
					radioButton.setAlignmentX(Component.CENTER_ALIGNMENT);
					memeOutputPanel.add(radioButton);
				}
				
				/*
				 * We use the pack command in hope of making the MEME outputs
				 * more easily visible.
				 * I think the setSize command is quite irrelevant since I think
				 * the pack command takes care of all sizing.
				 */
				fimoFrame.pack();
			}
		}	
	}
}