package de.lg.syncvis.internal;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.InputStreamReader;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

/**
 * This class is adopted from
 * https://github.com/cytoscape/cytoscape-samples/blob
 * /master/sample-use-task-monitor
 * /src/main/java/org/cytoscape/sample/internal/UseTaskMonitorTask.java
 * 
 * This class is much under development.
 * 
 * @last update: Nov 10, 2017
 */
public class PerformDeAnalMonitorTask extends AbstractTask {
	
	/*
	 * We use excepOccured and galRespStr to store a successful Galaxy response
	 * and error message, respectively.
	 * 
	 * In the end (=finalize method), we show a message in a separate pop-up
	 * window based on the above mentioned variables.
	 * 
	 * OBS!
	 * I think we should not use JOptionPane.showMessageDialog for this purpose
	 * since it seems to cause a conflict with taskMonitor, the taskMonitor will
	 * be handing forever. 
	 */
	
	static Logger logger = LogManager.getLogger(
		PerformDeAnalMonitorTask.class.getName()
	);
	MyGalCytoPanel mainApp;
	public PerformDeAnalMonitorTask(MyGalCytoPanel mainApp) {
		this.mainApp = mainApp;
	}
	
	@Override
	public void run(final TaskMonitor taskMonitor) {
		
		taskMonitor.setTitle(
			"Performing Differiantial Gene Expression Analysis ..."
		);
		
		/*
		 * The following link may contain useful statistical Galaxy methods: 
		 * https://toolshed.g2.bx.psu.edu/repository?repository_id=c8c1439403cf4a69
		 */
		
		/*
		 * I think the idea is that we should first call
		 * getgaltoolpanelmonitor.py in order to automatically parse relevant
		 * statistical tools from Galaxy and from which the user should select
		 * proper statistical tool for performdeanalmonitortask.py. 
		 * 
		 * We could use JTree for displaying the parsed statistical tools,
		 * perhaps something similar as we did in plotGenes.
		 */
		
		/*
		 * It seems taskMonitor.setProgress(0.1) would freeze the progress bar
		 * which would look like an error occurred, so it is probably good not to
		 * use it.
		 */
		// taskMonitor.setProgress(0.1);
		
		String galRespStr = "";
		boolean excepOccured = false;
		try {
			
			String prg = Util.readFile(
				"performdeanalmonitortask.py",
				mainApp.currentGalUrl,
				mainApp.currentGalApiKey
			);
			
			BufferedWriter out = new BufferedWriter(
				new FileWriter("performDeAnal.py")
			);
			out.write(prg);
			out.close();
			
			Process p = Runtime.getRuntime().exec("python performDeAnal.py");
			BufferedReader in = new BufferedReader(new InputStreamReader(
				p.getInputStream()
			));
			galRespStr = in.readLine();
			
			
			
		} catch (Exception e2) {
			excepOccured = true;
			logger.error("e2: " + e2);
			
		} finally {
			taskMonitor.setProgress(1.0);
			
			/*
			 * In the end we open a separate pop-up window in order to show a
			 * successful response or an error message.
			 */
			JFrame outputFrame = new JFrame();
			outputFrame.setSize(600,720);
			JEditorPane outputPane = new JEditorPane();
			JScrollPane outputScrollPane = new JScrollPane(outputPane);
			outputPane.setEditable(false);
			outputPane.setContentType("text/html");
			String outputHtml = "";
			
			if (excepOccured || galRespStr == null || galRespStr.length() == 0) {
				
				outputHtml =
					"<html><font color='red'>" +
					"An error occured when accessing the Galaxy Server." +
					"<br>" +
					"Possible reasons for the error:" +
					"<ul>" +
					"<li>" +
					"You are not running Cytoscape as administrator." +
					"</li>" +
					"<li>" +
					"'Galaxy Server URL' or 'Galaxy Server API KEY' is wrong." +
					"</li>" +
					"<li>" +
					"Galaxy Server is down." +
					"</li>" +
					"<li>" +
					"Your internet connection is not working." +
					"</li>" +
					"</ul>" +
					"</font></html>";
				
			} else {
				
				/*
				 * TODO
				 * If we change galRespStr to be another variable type,
				 * then we may have to do some modifications here.
				 */
				outputHtml = galRespStr;
			}
			outputPane.setText(outputHtml);
			outputFrame.getContentPane().add(outputScrollPane);
			outputFrame.setVisible(true);
			outputFrame.pack();
			outputFrame.toFront();
		}
	}
}