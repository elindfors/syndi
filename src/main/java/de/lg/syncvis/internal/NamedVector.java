package de.lg.syncvis.internal;

import java.util.Vector;

/**
 * This class is adopted from
 * http://www.java2s.com/Code/Java/Swing-JFC/CheckBoxNodeTreeSample.htm ->
 * NamedVector
 * 
 * @author: Erno Lindfors
 * @last update: Apr 29, 2016
 */
public class NamedVector extends Vector {
	private static final long serialVersionUID = 947117508694091505L;
	String name;
	
	public NamedVector(String name) {
		this.name = name;
	}
	public NamedVector(String name, Object elements[]) {
		this.name = name;
		for (int i = 0, n = elements.length; i < n; i++) {
			add(elements[i]);
		}
	}
	public String toString() {
		return "[" + name + "]";
	}
}