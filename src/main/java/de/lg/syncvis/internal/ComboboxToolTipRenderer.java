package de.lg.syncvis.internal;

import java.awt.Component;
import java.util.List;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JComponent;
import javax.swing.JList;
import javax.swing.JToolTip;

/**
 * This implementation is adapted from
 * http://stackoverflow.com/questions/480261/
 * java-swing-mouseover-text-on-jcombobox-items.
 * 
 * @author: Erno Lindfors
 * @last update: Dec 04, 2014
 */
public class ComboboxToolTipRenderer extends DefaultListCellRenderer
{
	private static final long serialVersionUID = -3883181393888732987L;
	List<JToolTip> tooltips;
	
	@Override
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus)
	{
		JComponent comp = (JComponent) super.getListCellRendererComponent(list,value,index,isSelected,cellHasFocus);
		
		if (-1 < index && null != value && null != tooltips)
		{
			// list.setToolTipText(tooltips.get(index));
			list.setToolTipText(tooltips.get(index).getTipText());
		}
		return comp;
	}
	
	// public void setTooltips(ArrayList<JToolTip> tooltips) {
	public void setTooltips(List<JToolTip> tooltips)
	{
		// public void setTooltips(ArrayList tooltips) {
		this.tooltips = tooltips;
	}
}
