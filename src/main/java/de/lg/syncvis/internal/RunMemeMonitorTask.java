package de.lg.syncvis.internal;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JScrollPane;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

/**
 * This class is adopted from
 * https://github.com/cytoscape/cytoscape-samples/blob
 * /master/sample-use-task-monitor
 * /src/main/java/org/cytoscape/sample/internal/UseTaskMonitorTask.java
 * 
 * @last update: Mar 21, 2018
 */
public class RunMemeMonitorTask extends AbstractTask {
	
	/*
	 * We use excepOccured and galRespStr to store a successful Galaxy response
	 * and error message, respectively.
	 * 
	 * In the end (=finalize method), we show a message in a separate pop-up
	 * window based on the above mentioned variables.
	 * 
	 * OBS!
	 * I think we should not use JOptionPane.showMessageDialog for this purpose
	 * since it seems to cause a conflict with taskMonitor, the taskMonitor will
	 * be handing forever. 
	 */
	
	static Logger logger = LogManager.getLogger(
		RunMemeMonitorTask.class.getName()
	);
	MyGalCytoPanel mainApp;
	public RunMemeMonitorTask(MyGalCytoPanel mainApp) {
		this.mainApp = mainApp;
	}
	
	@Override
	public void run(final TaskMonitor taskMonitor) throws Exception {
		
		/*
		 * It seems in this method we should not give pop-up messages since it
		 * seems they would lead to infinite loops.
		 * 
		 * We therefore should not use pop-up windows for debugging but it is ok
		 * to print debugs directly to log files.
		 * 
		 * Also, we cannot give any pop-up messages about missing input
		 * parameters.
		 */
		taskMonitor.setTitle("Running MEME ...");
		
		/*
		 * It seems taskMonitor.setProgress(0.1) would freeze the progress bar
		 * which would look like an error occurred, so it is probably good not to
		 * use it.
		 */
		// taskMonitor.setProgress(0.1);
		
		String galRespStr = "";
		boolean excepOccured = false;
		try {
			
			/*
			 * Create a temporary file for input genes.
			 *
			 * OBS!
			 * It seems "\n" does not generate line break,
			 * instead "line.separator" is needed.
			 */
			String genesString = "";
			List<String> selPlotGenesList =
				new ArrayList<String>(mainApp.selMemeGenes);
			for (int i = 0; i < selPlotGenesList.size(); i++) {
				if (i > 0) {
					genesString = genesString.concat(System.getProperty(
						"line.separator"
					));
				}
				genesString = genesString.concat(selPlotGenesList.get(i));
			}
			OutputStream fos = new FileOutputStream(
				System.getProperty("user.dir") + "/tempMemeGenes.txt"
			);
			PrintWriter foWriter = new PrintWriter(fos);
			foWriter.print(genesString);
			foWriter.close();
			
			/*
			 * If the user has selected genes, then we modify seqString so that it
			 * contains only these genes.
			 */
//			if (mainApp.selMemeGenes != null
//					|| mainApp.selMemeGenes.size() > 0
//			) {
//				String[] seqStringRows = mainApp.memeSeqString.split("\n");
//				String newSeqString = "";
//				boolean hasContent = false;
//				for (int i = 0; i < seqStringRows.length; i++) {
//					/*
//					 * TODO
//					 * At the moment we assume the gene id is in the second
//					 * column appended by semicomma (;).
//					 * I think we should find a more elegant way to identify it.
//					 */
//					String row = seqStringRows[i];
//					String[] cols = row.split("\t");
//					if (cols == null || cols.length < 2) {
//						continue;
//					}
//					String secondCol = cols[1].trim();
//					String gene = secondCol.split(";")[0].trim();
//					if (gene == null || gene.length() == 0) {
//						continue;
//					}
//					if (hasContent) {
//						newSeqString = newSeqString.concat("\n");
//					}
//					if (mainApp.selMemeGenes.contains(gene)) {
//						newSeqString = newSeqString.concat(gene);
//						hasContent = true;
//					}
//				}
//			}
			
			/*
			 * We store mainApp.memeGenomeAnnotStrg in a temporary file.
			 */
			FileUtils.copyFile(
				mainApp.memeGenomeAnnotFile,
				new File(
					System.getProperty("user.dir") +
					"/tempMemeGenomeAnnotation.fasta"
				)
			);
			
			/*
			 * Call galaxy by using the input text files.
			 * 
			 * It seems this works now. However it seems the outputUrl is available
			 * much earlier than the Galaxy process is completed, so if the user
			 * clicks the outputUrl immediately (s)he will quite likely get an empty
			 * file.
			 */
			String prg = Util.readFile(
				"mememonitor.py",
				mainApp.currentGalUrl,
				mainApp.currentGalApiKey
			);
			BufferedWriter out = new BufferedWriter(new FileWriter(
				"runMeme.py"
			));
			out.write(prg);
			out.close();
			
			Process p = Runtime.getRuntime().exec("python runMeme.py");
			BufferedReader in = new BufferedReader(
				new InputStreamReader(p.getInputStream())
			);
			galRespStr = in.readLine();
			
		} catch (Exception e2) {
			excepOccured = true;
			logger.error("e2: " + e2);
		} finally {
			taskMonitor.setProgress(1.0);
			
			/*
			 * In the end we open a separate pop-up window in order to show a
			 * successful response or an error message.
			 */
			JFrame outputFrame = new JFrame();
			outputFrame.setSize(600,720);
			JEditorPane outputPane = new JEditorPane();
			JScrollPane outputScrollPane = new JScrollPane(outputPane);
			outputPane.setEditable(false);
			outputPane.setContentType("text/html");
			String outputHtml = "";
			boolean galTaskSuccessful = false;
			if (excepOccured || galRespStr == null || galRespStr.length() == 0) {
				galTaskSuccessful = false;
				outputHtml =
					"<html><font color='red'>" +
					"An error occured when accessing the Galaxy Server." +
					"<br>" +
					"Possible reasons for the error:" +
					"<ul>" +
					"<li>" +
					"You are not running Cytoscape as administrator." +
					"</li>" +
					"<li>" +
					"'Galaxy Server URL' or 'Galaxy Server API KEY' is wrong." +
					"</li>" +
					"<li>" +
					"Galaxy Server is down." +
					"</li>" +
					"<li>" +
					"Your internet connection is not working." +
					"</li>" +
					"</ul>" +
					"</font></html>";
			} else {
				galTaskSuccessful = true;
				outputHtml =
					"<html><body>The MEME is now completed.<br>" +
					"Your web browser should shortly be opened on which you " +
					"should be directed to the result page.<br>" +	
					"If this does not work, please open a web browser by " +
					"yourself and open the following URL on it:<br>" +
					// "yourself and open either of the following URLs on it:<br>" +					
					"<a href='" + galRespStr + "display/?preview=True'>" +
					galRespStr + "display/?preview=True</a><br><br><br>" +
					// "<a href='" + galRespStr + "'>" + galRespStr + "</a><br><br>" +
					"<font color='red'>If the result page is empty, then it " +
					"is probably a sign the results are not yet loaded on " +
					"your browser.<br>" +
					"In this case please wait a second and then access the " +
					"page again.</font></body></html>";
			}
			outputPane.setText(outputHtml);
			outputFrame.getContentPane().add(outputScrollPane);
			outputFrame.setVisible(true);
			outputFrame.pack();
			outputFrame.toFront();
			if (galTaskSuccessful) {
				UrlOnWebBrowserOpener.process(
					galRespStr + "display/?preview=True"
				);
			}
		}
	}
}