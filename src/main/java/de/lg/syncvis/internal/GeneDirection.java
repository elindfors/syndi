package de.lg.syncvis.internal;

public class GeneDirection
{
  public String geneId;
  public boolean direction;
  
  public GeneDirection(String geneId, boolean direction)
  {
    this.geneId = geneId;
    this.direction = direction;
  }
  
}
