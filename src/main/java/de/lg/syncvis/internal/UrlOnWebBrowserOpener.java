package de.lg.syncvis.internal;

import java.awt.Desktop;
import java.net.URI;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class is for opening the content of an URL automatically on a web
 * browser.
 * 
 * Many ideas are adopted from
 * https://stackoverflow.com/questions/5226212/how-to-open-the-default-webbrowser-using-java 
 * 
 * I believe it most pragmatic to have this implementation in one class instead
 * of having it separately at each MonitorTask.java since it can happen so that
 * we have to modify this implementation every now and then (e.g. this may never
 * never work perfectly). 
 * 
 * @last update: Nov 02, 2017
 */
public class UrlOnWebBrowserOpener {
	private static final long serialVersionUID = 947117508694091505L;
	static Logger logger = LogManager.getLogger(
		UrlOnWebBrowserOpener.class.getName()
	);
	
	public static void process(String url) throws Exception {
		
		/*
		 * We implement this separately for Windows, Ubuntu and Mac.
		 */
		String os = System.getProperty("os.name").toLowerCase();
		if (os.indexOf("win") >= 0) {
        	
			/*
        	 * This window implementation has not been tested but at least
        	 * according to in the beginning linked web page this should work.
        	 */
			logger.debug("The user is using Windows");
        	Desktop.getDesktop().browse(new URI(url));
        } else if (os.indexOf("nix") >=0 || os.indexOf("nux") >=0) {
        	logger.debug("The user is using Ubuntu");
        	
        	/*
        	 * We try the opening on various browsers:
        	 * If the first did not work, then we try the next browser and so
        	 * on.
        	 */
        	Runtime rt = Runtime.getRuntime();
        	String[] browsers = {
        	    "google-chrome",
    			"epiphany",
    			"firefox",
    			"mozilla",
    			"konqueror",
                "netscape",
                "opera",
                "links",
                "lynx"
            };
            StringBuffer cmd = new StringBuffer();
			for (int i = 0; i < browsers.length; i++) {
			    if (i == 0) {
			        cmd.append(String.format("%s \"%s\"", browsers[i], url));
        		} else {
			        cmd.append(String.format(" || %s \"%s\"", browsers[i], url));
        		}
			}
			logger.debug("cmd.toString(): " + cmd.toString());
			rt.exec(new String[] { "sh", "-c", cmd.toString() });
        	
        } else if (os.indexOf("mac") >= 0) {
        	
        	/*
        	 * This mac implementation has not been tested but at least according
        	 * to in the beginning linked web page this should work.
        	 */
        	logger.debug("The user is using Mac");
        	Runtime rt = Runtime.getRuntime();
        	rt.exec("open " + url);
        }
	}
}