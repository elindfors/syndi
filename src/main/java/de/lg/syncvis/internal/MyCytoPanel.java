package de.lg.syncvis.internal;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.cytoscape.application.swing.CytoPanelComponent;
import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.model.CyEdge;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyRow;
import org.cytoscape.model.CyTable;
import org.cytoscape.model.CyTableUtil;
import org.cytoscape.model.events.RowSetRecord;
import org.cytoscape.model.events.RowsSetEvent;
import org.cytoscape.view.model.CyNetworkView;

/**
 * 
 * 
 * @last update: Dec 06, 2017
 */
public class MyCytoPanel extends JPanel implements CytoPanelComponent {
	private static final long serialVersionUID = 947117508694091505L;
	
	static Logger logger = LogManager.getLogger(MyCytoPanel.class.getName());
	private CyActivator master;
	
	/*
	 * We do need remHighlightProcessing and nodeSelectionProcessing
	 * for preventing calling MyRowsSetListener.handleEvent(RowsSetEvent e)
	 * in vain.
	 * For some reason this method is triggered when nodes are removed and
	 * selected.
	 */
	int ignoreSelectionEvents = 0;
	
	final static String YES = "yes";
	final static String NO = "no";
	
	public boolean qPressed;
	public boolean wPressed;
	
	private static JButton remDuplEdgesButton;
	// private static JButton highlightNodesInOtherNetsButton;
	private static JButton copySelGenesToClipBoardButton;
	private static JButton saveSelectedGenesInFileButton;
	private static JButton importSelGenesButton;
	private static JButton remAllSelsButton;
	
	// static List<CyNode> selNodesAtPrevRowSetsEvent = new ArrayList<CyNode>();
	private HashSet<String> currentSelection = new HashSet<String>();
	HashMap<String, List<String>> geneHomologs = new HashMap<String, List<String>>();
	JTabbedPane mappingPane;
	JPanel mappingIgnoreCasePanel;
	JPanel filePanel;
	
	static JComboBox<String> mappingIgnoreCaseComboBox;
	// static JComboBox<String> nodeSelModeComboBox;
	
	public MyCytoPanel(CyActivator master) {
		this.master = master;
		
		int wholeWidth = 500;
		int wholeHeight = 1000;
		this.setPreferredSize(new Dimension(wholeWidth, wholeHeight));
		
		remAllSelsButton = new JButton("Remove All Selections");
		remAllSelsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				remAllSelections();
			}
		});
		JPanel remAllSelsPanel = new JPanel();
		GroupLayout remAllSelsLayout = new GroupLayout(remAllSelsPanel);
		remAllSelsLayout.setAutoCreateGaps(true);
		remAllSelsLayout.setAutoCreateContainerGaps(true);
		remAllSelsLayout.setVerticalGroup(
			remAllSelsLayout.createSequentialGroup().addGroup(
				remAllSelsLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(remAllSelsButton)
			)
		);
		remAllSelsLayout.setHorizontalGroup(
			remAllSelsLayout.createSequentialGroup()
			.addGroup(remAllSelsLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
	        	.addComponent(remAllSelsButton)
        	)
		);
		remAllSelsPanel.setLayout(remAllSelsLayout);
		
		remDuplEdgesButton = new JButton("Remove Duplicate Edges");
		remDuplEdgesButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				removeDuplicateEdges();
			}
		});
		JPanel remDuplEdgesPanel = new JPanel();
		GroupLayout remDuplEdgesLayout = new GroupLayout(remDuplEdgesPanel);
		remDuplEdgesLayout.setAutoCreateGaps(true);
		remDuplEdgesLayout.setAutoCreateContainerGaps(true);
		remDuplEdgesLayout.setVerticalGroup(
			remDuplEdgesLayout.createSequentialGroup().addGroup(
				remDuplEdgesLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(remDuplEdgesButton)
			)
		);
		remDuplEdgesLayout.setHorizontalGroup(
			remDuplEdgesLayout.createSequentialGroup()
			.addGroup(remDuplEdgesLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
	        	.addComponent(remDuplEdgesButton)
        	)
		);
		remDuplEdgesPanel.setLayout(remDuplEdgesLayout);
		
		/*
		 * highlightNodesInOtherNetsButton is removed at least for the moment.
		 * However I think is good to keep the code available (i.e. ignore it)
		 * since we might have to switch back using it since the alternative
		 * implementation at MyRowsSetListener.handleEvent tends to be a bit
		 * unstable.
		 */
		/*
		highlightNodesInOtherNetsButton = new JButton(
			"Highlight Selected Nodes In Other Networks"
		);
		highlightNodesInOtherNetsButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				highLightNodesInOtherNets();
			}
		});
		
		JPanel highlightNodesInOtherNetsPanel = new JPanel();
		GroupLayout highlightNodesInOtherNetsLayout = new GroupLayout(highlightNodesInOtherNetsPanel);
		highlightNodesInOtherNetsLayout.setAutoCreateGaps(true);
		highlightNodesInOtherNetsLayout.setAutoCreateContainerGaps(true);
		highlightNodesInOtherNetsLayout.setVerticalGroup(
			highlightNodesInOtherNetsLayout.createSequentialGroup().addGroup(
				highlightNodesInOtherNetsLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(highlightNodesInOtherNetsButton)
			)
		);
		highlightNodesInOtherNetsLayout.setHorizontalGroup(
			highlightNodesInOtherNetsLayout.createSequentialGroup()
				.addGroup(highlightNodesInOtherNetsLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
	        	.addComponent(highlightNodesInOtherNetsButton)
        	)
		);
		highlightNodesInOtherNetsPanel.setLayout(highlightNodesInOtherNetsLayout);
		*/
		
		mappingIgnoreCaseComboBox = new JComboBox<String>();
		mappingIgnoreCaseComboBox.addItem(YES);
		mappingIgnoreCaseComboBox.addItem(NO);
		mappingIgnoreCaseComboBox.setSelectedItem(YES);
		
		JLabel ignoreCaseLabel = new JLabel("Ignore Case");
		// JLabel ignoreCaseLabel = new JLabel("Node Mapping - Ignore Case");
		mappingIgnoreCasePanel = new JPanel();
		GroupLayout groupLayout = new GroupLayout(mappingIgnoreCasePanel);
		groupLayout.setAutoCreateGaps(true);
		groupLayout.setAutoCreateContainerGaps(true);
		groupLayout.setVerticalGroup(
			groupLayout.createSequentialGroup().addGroup(
				groupLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(ignoreCaseLabel)
					.addComponent(mappingIgnoreCaseComboBox)
			)
		);
		groupLayout.setHorizontalGroup(groupLayout.createSequentialGroup()
	         .addComponent(ignoreCaseLabel)
	         .addComponent(mappingIgnoreCaseComboBox)
		);
		mappingIgnoreCasePanel.setLayout(groupLayout);
		
		JLabel label1 = new JLabel("Node Selection Mode:");
		JLabel label2 = new JLabel("default : NEW");
		JLabel label3 = new JLabel("'ctrl' down : OR");
		JLabel label4 = new JLabel("'q' down AND");
		JLabel label5 = new JLabel("'w' down XOR"); 
		JPanel nodeSelModePanel = new JPanel();
		GroupLayout nodeSelModeLayout = new GroupLayout(nodeSelModePanel);
		nodeSelModeLayout.setAutoCreateGaps(true);
		nodeSelModeLayout.setAutoCreateContainerGaps(true);
		nodeSelModeLayout.setVerticalGroup(
			nodeSelModeLayout.createSequentialGroup()
				.addComponent(label1)
				.addComponent(label2)
				.addComponent(label3)
				.addComponent(label4)	
				.addComponent(label5)
		);
		nodeSelModeLayout.setHorizontalGroup(nodeSelModeLayout.createSequentialGroup().addGroup(
			nodeSelModeLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
	        	.addComponent(label1)
	        	.addComponent(label2)
	        	.addComponent(label3)
	        	.addComponent(label4) 
	        	.addComponent(label5) 
	    ));
		nodeSelModePanel.setLayout(nodeSelModeLayout);
		
		copySelGenesToClipBoardButton = new JButton("Copy Selected Genes to Clipboard");
		copySelGenesToClipBoardButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				selectedGenesToClipBoard();
			}
		});
		JPanel copyPanel = new JPanel();
		GroupLayout copyLayout = new GroupLayout(copyPanel);
		copyLayout.setAutoCreateGaps(true);
		copyLayout.setAutoCreateContainerGaps(true);
		copyLayout.setVerticalGroup(
			copyLayout.createSequentialGroup().addGroup(
				copyLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(copySelGenesToClipBoardButton)
			)
		);
		copyLayout.setHorizontalGroup(
			copyLayout.createSequentialGroup()
			.addGroup(copyLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
	        	.addComponent(copySelGenesToClipBoardButton)
        	)
		);
		copyPanel.setLayout(copyLayout);
		
		saveSelectedGenesInFileButton = new JButton("Save Selected Genes In File");
		saveSelectedGenesInFileButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveSelectedGenesInFile();
			}
		});
		JPanel savePanel = new JPanel();
		GroupLayout saveLayout = new GroupLayout(savePanel);
		saveLayout.setAutoCreateGaps(true);
		saveLayout.setAutoCreateContainerGaps(true);
		saveLayout.setVerticalGroup(
			saveLayout.createSequentialGroup().addGroup(
				saveLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(saveSelectedGenesInFileButton)
			)
		);
		saveLayout.setHorizontalGroup(
			saveLayout.createSequentialGroup()
			.addGroup(saveLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
	        	.addComponent(saveSelectedGenesInFileButton)
        	)
		);
		savePanel.setLayout(saveLayout);
		
		importSelGenesButton = new JButton("Import Selected Genes From File");
		importSelGenesButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				importSelGenes();
			}
		});
		JPanel importSelGenesPanel = new JPanel();
		GroupLayout importSelGenesLayout = new GroupLayout(importSelGenesPanel);
		importSelGenesLayout.setAutoCreateGaps(true);
		importSelGenesLayout.setAutoCreateContainerGaps(true);
		importSelGenesLayout.setVerticalGroup(
			importSelGenesLayout.createSequentialGroup().addGroup(
				importSelGenesLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(importSelGenesButton)
			)
		);
		importSelGenesLayout.setHorizontalGroup(
			importSelGenesLayout.createSequentialGroup()
			.addGroup(importSelGenesLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
	        	.addComponent(importSelGenesButton)
        	)
		);
		importSelGenesPanel.setLayout(importSelGenesLayout);
		
		JButton uploadHiglightMappingFileButton = new JButton("Upload Mapping File");
		filePanel = new JPanel();
		GroupLayout fileGroupLayout = new GroupLayout(filePanel);
		fileGroupLayout.setAutoCreateGaps(true);
		fileGroupLayout.setAutoCreateContainerGaps(true);
		fileGroupLayout.setVerticalGroup(
			fileGroupLayout.createSequentialGroup().addGroup(
				fileGroupLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(uploadHiglightMappingFileButton)
			)
		);
		
		fileGroupLayout.setHorizontalGroup(
			fileGroupLayout.createSequentialGroup()
			.addGroup(fileGroupLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
	        	.addComponent(uploadHiglightMappingFileButton)
        	)
		);
		filePanel.setLayout(fileGroupLayout);
		
		uploadHiglightMappingFileButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				JFileChooser chooser = new JFileChooser();
				FileNameExtensionFilter filter =
					new FileNameExtensionFilter("*.txt","txt");
				chooser.addChoosableFileFilter(filter);
				int returnVal = chooser.showOpenDialog(null);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File mappingFile = chooser.getSelectedFile();
					Scanner mappingScanner = null;
					try {
						mappingScanner = new Scanner(mappingFile);
					} catch (Exception e1) {
						logger.error(e1);
					}					
					while (mappingScanner.hasNextLine()) {
						String row = mappingScanner.nextLine();
						String[] cols = row.split("\t");
						
						String gene1 = cols[0].trim();
						String gene2 = cols[1].trim();
						
						List<String> homologs1 = geneHomologs.get(gene1);
						if (homologs1 == null) {
							homologs1 = new ArrayList<String>();
						}
						homologs1.add(gene2);
						geneHomologs.put(gene1, homologs1);
						
						List<String> homologs2 = geneHomologs.get(gene2);
						if (homologs2 == null) {
							homologs2 = new ArrayList<String>();
						}
						homologs2.add(gene1);
						geneHomologs.put(gene2, homologs2);
					} 
					mappingScanner.close();	
				}
			}
		});
		mappingPane = new JTabbedPane();
		mappingPane.addTab(
			"Map with shared names",
			null,
			mappingIgnoreCasePanel,
            "Node mapping happens with 'shared name' attributes."
		);
		mappingPane.addTab(
			"Map with file",
			null,
			filePanel,
            "Node mapping happens from a file."
		);
		
		/*
		 * The following setPreferredSize enforces the tabs to be aligned
		 * horizontally. 
		 */
		mappingPane.setPreferredSize(new Dimension(400, 50));
		
		JPanel wholePanel = new JPanel();
		GroupLayout wholeLayout = new GroupLayout(wholePanel);
		wholeLayout.setAutoCreateGaps(true);
		wholeLayout.setAutoCreateContainerGaps(true);
		wholeLayout.setHorizontalGroup(
			wholeLayout.createSequentialGroup().addGroup(
				wholeLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
				    .addComponent(remAllSelsPanel)
					.addComponent(remDuplEdgesPanel)
 					.addComponent(savePanel)
 					.addComponent(importSelGenesPanel)
					.addComponent(copyPanel)
 					// .addComponent(highlightNodesInOtherNetsPanel)
 					.addComponent(nodeSelModePanel)
 					.addComponent(mappingPane)
			)
		);
		wholeLayout.setVerticalGroup(wholeLayout.createSequentialGroup()
			 .addComponent(remAllSelsPanel)
			 .addComponent(remDuplEdgesPanel)
	         .addComponent(savePanel)
	         .addComponent(importSelGenesPanel)
             .addComponent(copyPanel)
             // .addComponent(highlightNodesInOtherNetsPanel)
             .addComponent(nodeSelModePanel)
             .addComponent(mappingPane)
		);
		wholePanel.setLayout(wholeLayout);
        this.add(wholePanel);
	}
	
	public Component getComponent() {
		return this;
	}
	public CytoPanelName getCytoPanelName() {
		return CytoPanelName.WEST;
	}
	public String getTitle() {
		// return "SyNDI - Vis";
		return "SyncVis - Net";
	}
	public Icon getIcon() {
		return null;
	}
	
	private void remAllSelections() {
		logger.debug("remAllSelsButton clicked");
		
		/* Retrieve currently existing networks. */
		Set<CyNetwork> cyNetSet = master.getAllNetworks();
		List<CyNetwork> cyNets = new ArrayList<CyNetwork>();
		cyNets.addAll(cyNetSet);
		
		/*
		 * We go through all nodes in all currently existing
		 * networks to remove all selections.
		 */
		for (int i = 0; i < cyNets.size(); i++) {
			CyNetwork cyNet = cyNets.get(i);
			CyTable cyNodeTable = cyNet.getDefaultNodeTable();
			List<CyNode> cyNodes = cyNet.getNodeList();
			for (int j = 0; j < cyNodes.size(); j++) {
				CyNode cyNode = cyNodes.get(j);
				if (cyNodeTable.getColumn(
					  "selected"
				) == null) {
				  cyNodeTable.createColumn(
					  "selected",
					  Boolean.class,
					  false
				  );
			  }
			  cyNet.getRow(cyNode).set(
				  "selected", false
			  );
			}
			
			/*
			 * Maybe at this point it is good to add the
			 * cyNet to NetworkManager
			 * since otherwise I think the recently made
			 * highlights would not be visible.
			 */
			CyNetworkView newCyNetView =
				master.getAdapter().getCyNetworkViewFactory().createNetworkView(
					cyNet
				);
			newCyNetView.updateView();
		  }
		  JOptionPane.showMessageDialog(
			  null,
			  "All node selections removed."
		  );
	}
	
	private void removeDuplicateEdges() {
		logger.debug("remDuplEdgesButton clicked");
		Set<CyNetwork> cyNetSet = master.getAllNetworks();
		List<CyNetwork> cyNets = new ArrayList<CyNetwork>();
		cyNets.addAll(cyNetSet);
		for (int i = 0; i < cyNets.size(); i++) {
			CyNetwork cyNet = cyNets.get(i);
			/*
			 * The following hash maps is for filtering out duplicate edges
			 * (=same nodes ignoring the directionality).
			 */
			HashMap<CyNode, List<CyNode>> nodeHandledNeighNodes =
				new HashMap<CyNode, List<CyNode>>();
			List<CyEdge> excessEdges = new ArrayList<CyEdge>();
			List<CyEdge> cyEdges = cyNet.getEdgeList();
			for (int j = 0; j < cyEdges.size(); j++) {
				CyEdge cyEdge = cyEdges.get(j);
				CyNode sNode = cyEdge.getSource();
				CyNode tNode = cyEdge.getTarget();
				
				/*
				 * If an edge with source and target node is already handled,
				 * then we skip this this iteration step and thus not consider
				 * the edge as an excess edge.
				 */
				List<CyNode> neighbNodes = nodeHandledNeighNodes.get(sNode);
				if (neighbNodes != null && neighbNodes.contains(tNode)) {
					excessEdges.add(cyEdge);
				}
				neighbNodes = nodeHandledNeighNodes.get(tNode);
				if (neighbNodes != null && neighbNodes.contains(sNode)) {
					excessEdges.add(cyEdge);
				}
				/*
				 * In the end we add source and target nodes to handled nodes.
				 */
				neighbNodes = nodeHandledNeighNodes.get(sNode);
				if (neighbNodes == null) {
					neighbNodes = new ArrayList<CyNode>();
				}
				neighbNodes.add(tNode);
				nodeHandledNeighNodes.put(sNode,neighbNodes);
				
				neighbNodes = nodeHandledNeighNodes.get(tNode);
				if (neighbNodes == null) {
					neighbNodes = new ArrayList<CyNode>();
				}
				neighbNodes.add(sNode);
				nodeHandledNeighNodes.put(tNode,neighbNodes);
			}
			
			if (excessEdges != null && excessEdges.size() == 0) {
				JOptionPane.showMessageDialog(
					null,
					"Network: " +
					cyNet.getRow(cyNet).get(CyNetwork.NAME,String.class) +
					" does not contain any duplicate edge."
				);
			}
			cyNet.removeEdges(excessEdges);
			CyNetworkView newCyNetView =
				master.getAdapter().getCyNetworkViewFactory().createNetworkView(
					cyNet
				);
			newCyNetView.updateView();
			
			JOptionPane.showMessageDialog(
				null,
				"Duplicate edge removal completed."
			);
		}
	}
		
	private void importSelGenes() {	
		List<String> fileGenes = new ArrayList<String>();
		JFileChooser chooser = new JFileChooser();
		FileNameExtensionFilter filter =
			new FileNameExtensionFilter("*.txt","txt");
		chooser.addChoosableFileFilter(filter);
		int returnVal = chooser.showOpenDialog(null);
		if (returnVal == JFileChooser.APPROVE_OPTION) {
			File importSelGenesFile = chooser.getSelectedFile();
			Scanner importSelGenesScanner = null;
			try {
				importSelGenesScanner = new Scanner(importSelGenesFile);
			} catch (Exception e1) {
				logger.error(e1);
			}					
			while (importSelGenesScanner.hasNextLine()) {
				String row = importSelGenesScanner.nextLine();
				fileGenes.add(row.trim().toUpperCase());
			} 
			importSelGenesScanner.close();
		}
		logger.debug("fileGenes: " + fileGenes);
		logger.debug("fileGenes.size(): " + fileGenes.size());
		
		Set<CyNetwork> cyNetSet = master.getAllNetworks();
		List<CyNetwork> cyNets = new ArrayList<CyNetwork>();
		cyNets.addAll(cyNetSet);
		
		/*
		 * The code snippet below is ignored at least for the moment since there
		 * is a bug when nodeSelModeComboBox "or", more details about the bug in
		 * the middle of snippet.
		 * 
		 * For the moment we implement the node importing assuming
		 * nodeSelModeComboBox, in practice this means all existing node
		 * selections will disappear when node selections are imported from a
		 * file. 
		 */
		Set<String> newSelNodeLabels = new HashSet<String>();
		newSelNodeLabels.addAll(fileGenes);
//		if (nodeSelModeComboBox.getSelectedItem().equals("or")) {
//			
//			/*
//			 * In this case we retrieve selNodesAtPrevRowSetsEvent from
//			 * all cyNets before adding fileGenes.
//			 */
//			for (int i = 0; i < cyNets.size(); i++) {
//				CyNetwork cyNet = cyNets.get(i);
//				if (selNodesAtPrevRowSetsEvent == null || selNodesAtPrevRowSetsEvent.size() == 0) {
//					continue;
//				}
//				for (int j = 0; j < selNodesAtPrevRowSetsEvent.size(); j++) {
//					CyNode tempNode = selNodesAtPrevRowSetsEvent.get(j);
//					
//					logger.debug("tempNode: " + tempNode);
//					logger.debug("cyNet: " + cyNet);
//					logger.debug(
//						"cyNet.getRow(cyNet).get(CyNetwork.NAME, String.class): " +
//						cyNet.getRow(cyNet).get(CyNetwork.NAME, String.class)
//					);
//					
//					/*
//					 * On 1.6.2016 evening I remained to a problem that the
//					 * following exception happened in the next line:
//					 * 
//					 * DEBUG: tempNode: Node suid: 62
//					 * DEBUG: cyNet: Complement and Coagulation Cascades_4
//					 * DEBUG: cyNet.getRow(cyNet).get(CyNetwork.NAME, String.class):
//					 * Complement and Coagulation Cascades_4
//					 * Exception in thread "AWT-EventQueue-0"
//					 * java.lang.IllegalArgumentException: unrecognized (table entry):
//					 * Node suid: 62  (table name): USER
//					 */
//					if (!cyNet.getRow(tempNode).getTable().getColumn(
//							"shared_name"
//						).getType().equals(String.class)) {
//							continue;
//					}
//					logger.debug(
//						"cyNet.getRow(tempNode).getTable().getColumn('shared_name').getType(): " +
//						cyNet.getRow(tempNode).getTable().getColumn("shared_name").getType()
//					);
//					String newSelLabel = cyNet.getRow(tempNode).get(
//						"shared_name",String.class
//					);
//					if (newSelLabel == null) {
//						newSelLabel = cyNet.getRow(tempNode).get(
//							"shared name",String.class
//						);
//					}
//					if (newSelLabel != null && newSelLabel.length() > 0) {
//						newSelNodeLabels.add(newSelLabel);
//					}
//				}
//			}
//			newSelNodeLabels.addAll(fileGenes);
//		} else if (nodeSelModeComboBox.getSelectedItem().equals("new")) {
//			newSelNodeLabels.addAll(fileGenes);
//		}
		
		logger.debug("newSelNodeLabels: " + newSelNodeLabels);
		logger.debug("newSelNodeLabels.size(): " + newSelNodeLabels.size());
		
		/*
		 * In the end we select newSelNodeLabels in all cyNets.
		 */
		boolean atLeastOneGeneImported = false;
		for (int i = 0; i < cyNets.size(); i++) {
			CyNetwork cyNet = cyNets.get(i);
			CyTable cyNodeTable = cyNet.getDefaultNodeTable();
			List<CyNode> cyNetNodes = cyNet.getNodeList();
			for (int k = 0; k < cyNetNodes.size(); k++) {
				CyNode cyNetNode = cyNetNodes.get(k);
				String cyNetLabel = cyNet.getRow(cyNetNode).get(
					"shared_name",String.class
				);
				if (cyNetLabel == null || cyNetLabel.length() == 0) {
					cyNetLabel = cyNet.getRow(cyNetNode).get(
						"shared name",String.class
					);
				}
				if (cyNetLabel == null || cyNetLabel.length() == 0) {
					continue;
				}
				if (newSelNodeLabels.contains(cyNetLabel.toUpperCase())) {
					if (cyNodeTable.getColumn("selected") == null) {
						cyNodeTable.createColumn("selected",Boolean.class,false);
					}
					cyNet.getRow(cyNetNode).set("selected",true);
					atLeastOneGeneImported = true;
				} else {
					if (cyNodeTable.getColumn("selected") == null) {
						cyNodeTable.createColumn("selected",Boolean.class,false);
					}
					cyNet.getRow(cyNetNode).set("selected",false);
				}
			}
			
			/*
			 * I think at this point it is good to update the network view in
			 * order to get the new selections visible.
			 * 
			 * It seems the following line "adapter.getCyNetworkManager()..." got
			 * the updateView() working.
			 * More about this:
			 * https://groups.google.com
			 * /forum/#!topic/cytoscape-helpdesk/sj6oKZSl2mk
			 */
			master.getAdapter().getCyNetworkManager().addNetwork(cyNet);
			CyNetworkView cyNetView = master.getAdapter().
				getCyNetworkViewFactory().createNetworkView(cyNet);
			cyNetView.updateView();
		}
		
		if (!atLeastOneGeneImported) {
			JOptionPane.showMessageDialog(
				null,
				"Not any gene in the file is in the netwowrks.\n" +
				"Please make sure you are using the identifiers properly."
			);
			return;
		}
		JOptionPane.showMessageDialog(
			null,
			"The genes successfully imported."
		);
	}
	
	private void saveSelectedGenesInFile() {
		List<String> selNodeLabels = retrieveSelNodeSharedNames();
		String fileString = "";
		  for (int i = 0; i < selNodeLabels.size(); i++) {
			  if (i > 0) {
				  fileString = fileString.concat("\n");
			  }
			  fileString = fileString.concat(selNodeLabels.get(i));
		  }
		  
		  final JFileChooser fc = new JFileChooser();
		  int val = fc.showSaveDialog(null);
		  if (val == JFileChooser.APPROVE_OPTION) {
			  try {
				  BufferedWriter bw = 
					  new BufferedWriter(
						  new FileWriter(
							  fc.getSelectedFile()
						  )
					  );
				  bw.write(fileString);
				  bw.close();
			  } catch (Exception e1) {
				  logger.error(
					  "Exception occured when reading selected file:\n" +
					  e1.getMessage()
				  );
		  		  e1.printStackTrace(System.err);
		  	  }
		  }
		  File savedFile = fc.getSelectedFile();
		  JOptionPane.showMessageDialog(
			  null,
			  "Selected genes saved in\n" +
			  savedFile.getPath() + savedFile.getName()
		  );
	}
	
	private List<String> retrieveSelNodeSharedNames() {
		Set<CyNetwork> cyNetSet = master.getAllNetworks();
		List<CyNetwork> cyNets = new ArrayList<CyNetwork>();
		cyNets.addAll(cyNetSet);
		List<String> selSharedNames = new ArrayList<String>();
		for (int i = 0; i < cyNets.size(); i++) {
			CyNetwork cyNet = cyNets.get(i);
			selSharedNames.addAll(this.getSelectedNodeSharedNames(cyNet));
		}
		return selSharedNames;
	}
	
	private void selectedGenesToClipBoard() {
		List<String> selNodeLabels = retrieveSelNodeSharedNames();
		String stringToClipboard = "";
		for (int i = 0; i < selNodeLabels.size(); i++) {
			if (i > 0) {
				stringToClipboard = stringToClipboard.concat("\n");
			}
			stringToClipboard = stringToClipboard.concat(selNodeLabels.get(i));
		}
		
		/*
		 * The following code snippet is adopted from
		 * http://stackoverflow.com/questions/3591945/copying-to-clipboard-in-java
		 */
		StringSelection selection = new StringSelection(stringToClipboard);
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		clipboard.setContents(selection,selection);
		
		JOptionPane.showMessageDialog(
			null, "Selected genes copied to Clipboard."
		);
	}
	
	public CyActivator getMaster() {
		return this.master;
	}
	
	/*
	 * Update the selection according to new selection generated by Cytoscape, it
	 * uses the currentSelection variable to keep track of what was/is selected
	 * and then forwards the selection to the other networks 
	 */
	public void updateSelection() {
	  try {
	    this.ignoreSelectionEvents++;
	    int mode = 0;
	    if(this.wPressed)
	      mode = 2;
	    if(this.qPressed)
	      mode = 1;
	      
	      HashSet<String> newSelNodes = this.getSelectedNodeSharedNames();
	      if(newSelNodes == null)
	        return;
	      
	      // Check if something changed
	      if(newSelNodes.containsAll(currentSelection) &&
	          currentSelection.containsAll(newSelNodes))
	        return;
	      
	      logger.debug("Selection changed: " + newSelNodes.size());
	      
	      // New and or mode we just use the Cytoscape standard functionality
	      if (mode == 1) {        
	        newSelNodes.retainAll(currentSelection);        
	        logger.debug("and");
	      } else if (mode == 2) {
	        newSelNodes.removeAll(currentSelection);      
	        logger.debug("xor");
	      }
	      this.setSelection(newSelNodes);
	    } catch(Throwable th) {
	      logger.debug(th.getMessage());
	      th.printStackTrace();
	    } finally {
	      this.ignoreSelectionEvents--;
	    }
	}
	
	public HashSet<String> getCurrentSelection() {
		return this.currentSelection;
	}
	
	public void setSelection(HashSet<String> newSelNodes) {
	    currentSelection = newSelNodes;
	    logger.debug("selection change:");
	    for (String key : this.geneHomologs.keySet()) {
	      if (newSelNodes.contains(key))
	        newSelNodes.addAll(this.geneHomologs.get(key));
	    }
	    boolean ignoreCase =
			mappingIgnoreCaseComboBox.getSelectedItem().equals(YES);
	    if (ignoreCase) {
	      HashSet<String> temp = new HashSet<String>();
	      for(String item : newSelNodes) {
	        temp.add(item.toUpperCase());
	      }
	      newSelNodes = temp;
	    }
	    for (CyNetwork cyNet : master.getAllNetworks()) {
	      putSelectionToNet(cyNet, newSelNodes, ignoreCase);
	    }
	}
	
	private void putSelectionToNet(
			CyNetwork selCyNet,
			HashSet<String> newSelNodes,
			boolean ignoreCase
	) {
	   CyTable cyNodeTable = selCyNet.getDefaultNodeTable();
	   List<RowSetRecord> rowsChanged = new ArrayList<RowSetRecord>();
	   this.master.getEventHelper().silenceEventSource(cyNodeTable);
	   for (CyNode node : selCyNet.getNodeList()) {
		   CyRow row = cyNodeTable.getRow(node.getSUID());
		   String name = row.get("shared name", String.class);
		   if (name == null) {
			   name = row.get("shared_name", String.class);
		   }
		   if (ignoreCase) {
			   name = name.toUpperCase();
		   }
		   boolean newState = newSelNodes.contains(name);
		   if (row.get("selected", Boolean.class) != newState
				   && name != null
				   && name.length() > 0
		   ) {
			   logger.debug(name + " -> " + newState);
			   row.set("selected", newState);
			   rowsChanged.add(new RowSetRecord(
				   row, CyNetwork.SELECTED, newState, newState
			   ));
		   }
	   }
	   this.master.getEventHelper().unsilenceEventSource(cyNodeTable);
	   
	   // Fire event
	   RowsSetEvent event = new RowsSetEvent(cyNodeTable, rowsChanged);
	   this.master.getEventHelper().fireEvent(event);
	}
	
	public HashSet<String> getSelectedNodeSharedNames() {
	    CyNetwork selCyNet = null;
	    for (CyNetwork cyNet : master.getAllNetworks()) {
	      if (cyNet.getRow(cyNet).get(
	    		  CyNetwork.SELECTED, Boolean.class
		  ) == true) {
	        selCyNet = cyNet;
	        break;
	      }
	    }
	    if (selCyNet == null)  {
	      return null;
	    }
	    return this.getSelectedNodeSharedNames(selCyNet);
	}

	public HashSet<String> getSelectedNodeSharedNames(CyNetwork net) {
		CyTable cyNodeTable = net.getDefaultNodeTable();
		List<CyNode> newSelNodes = CyTableUtil.getNodesInState(net,"selected",true);
		HashSet<String> selSharedNames = new HashSet<String>();
		for (CyNode node : newSelNodes) {
			String sharedName = cyNodeTable.getRow(node.getSUID()).get(
					"shared name", String.class
			); 
			if (sharedName == null || sharedName.length() == 0) {
				sharedName = cyNodeTable.getRow(node.getSUID()).get(
					"shared_name", String.class
				);
			}
			if (sharedName != null && sharedName.length() > 0) {
				selSharedNames.add(sharedName);
			}
		}
		return selSharedNames;
	}
	
	public void setCurrentSelectMem(HashSet<String> newSet) {
		this.currentSelection = newSet;
	}
}