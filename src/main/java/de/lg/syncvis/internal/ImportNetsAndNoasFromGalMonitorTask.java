package de.lg.syncvis.internal;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.InputStreamReader;

import javax.swing.JEditorPane;
import javax.swing.JScrollPane;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.cytoscape.work.AbstractTask;
import org.cytoscape.work.TaskMonitor;

/**
 * This class is adopted from
 * https://github.com/cytoscape/cytoscape-samples/blob
 * /master/sample-use-task-monitor
 * /src/main/java/org/cytoscape/sample/internal/UseTaskMonitorTask.java
 * 
 * @last update: Nov 03, 2017
 */
public class ImportNetsAndNoasFromGalMonitorTask extends AbstractTask {
	
	/*
	 * We use excepOccured and galRespStr to store a successful Galaxy response
	 * and error message, respectively.
	 * 
	 * In the end (=finalize method), we show a message in a separate pop-up
	 * window based on the above mentioned variables.
	 * 
	 * OBS!
	 * I think we should not use JOptionPane.showMessageDialog for this purpose
	 * since it seems to cause a conflict with taskMonitor, the taskMonitor will
	 * be handing forever. 
	 */
	
	static Logger logger = LogManager.getLogger(
		ImportNetsAndNoasFromGalMonitorTask.class.getName()
	);
	MyGalCytoPanel mainApp;;
	public ImportNetsAndNoasFromGalMonitorTask(MyGalCytoPanel mainApp) {
		this.mainApp = mainApp;
	}
	
	@Override
	public void run(final TaskMonitor taskMonitor) {
		taskMonitor.setTitle("Importing Networks and Attributes ...");
		
		/*
		 * It seems taskMonitor.setProgress(0.1) would freeze the progress bar
		 * which would look like an error occurred, so it is probably good not to
		 * use it.
		 */
		// taskMonitor.setProgress(0.1);
		
		String galRespStr = "";
		boolean excepOccured = false;
		
		try {	
			String prg = Util.readFile(
				"importnetsandnoasmonitor.py",
				mainApp.currentGalUrl,
				mainApp.currentGalApiKey
			);
			BufferedWriter out = new BufferedWriter(
				new FileWriter("importNetsAndNoas.py")
			);
			out.write(prg);
			out.close();
			
			Process p = Runtime.getRuntime().exec("python importNetsAndNoas.py");
			BufferedReader in = new BufferedReader(
				new InputStreamReader(p.getInputStream())
			);
			String currentLine;
			boolean isFristLine = true;
			while ((currentLine = in.readLine()) != null) {
				if (!isFristLine) {
					galRespStr = galRespStr.concat("<br>");
				}
				galRespStr = galRespStr.concat(currentLine);
				isFristLine = false;
			}
			in.close();
			
		} catch (Exception e2) {
			excepOccured = true;
			logger.error("e2: " + e2);
			logger.debug("e2: " + e2);
			
		} finally {
			
			taskMonitor.setProgress(1.0);
			
			/*
			 * In the end we open a separate pop-up window in order to show a
			 * successful response or an error message.
			 */
			mainApp.importNetsAndNoasOutputFrame.setSize(600,720);
			JEditorPane outputPane = new JEditorPane();
			JScrollPane outputScrollPane = new JScrollPane(outputPane);
			outputPane.setEditable(false);
			outputPane.setContentType("text/html");
			String outputHtml = "";
			
			if (excepOccured || galRespStr == null || galRespStr.length() == 0) {
				outputHtml =
					"<html><font color='red'>" +
					"An error occured when accessing the Galaxy Server." +
					"<br>" +
					"Possible reasons for the error:" +
					"<ul>" +
					"<li>" +
					"You are not running Cytoscape as administrator." +
					"</li>" +
					"<li>" +
					"'Galaxy Server URL' or 'Galaxy Server API KEY' is wrong." +
					/*
					"Your settings under 'Configure Galaxy Server' button " +
					"are not correct." +
					*/
					"</li>" +
					"<li>" +
					"Galaxy Server is down." +
					"</li>" +
					"<li>" +
					"Your internet connection is not working." +
					"</li>" +
					"</ul>" +
					"</font></html>";
				
				/*
				 * The following is quite a dirty and unelegant way to check
				 * if there is not any CyNet and noa in Galaxy server.
				 * 
				 * We may want to consider a more elegant solution.
				 * For example shifting the message from xxx_monitor.py but it
				 * seems this is not trivial since for some reason the message
				 * seems to persistently remain in xxx_monitor.py.
				 */
			} else if (galRespStr.contains("and not any node att")) {
				outputHtml =
					"<html><font color='red'> " +
					galRespStr +
					"</font></html>";
			} else {
				
				outputHtml =
					"<html><font color='red'>The results are now " +
					"retrieved from galaxy.<br>" +
					"You can access them from the below listed links.<br>" +
					"They are also copied to clipboard from which you can " +
					"paste them e.g. to a text editor.</font><br><br>" +
					galRespStr +
					"<br><br><font color='red'>" +
					"If some of the result page is empty, then it is " +
					"probably a sign the results are not yet loaded on " +
					"your browser.<br>" +
					"In this case please wait a second and then click the " +
					"link again.</font></html>";
				
				/*
				 * It is also good to copy galRespStr to clipboard since it could
				 * happen so that outputFrame does not open properly on some
				 * computers.
				 */
				StringSelection selection = new StringSelection(galRespStr);
				Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
				clipboard.setContents(selection,selection);
				
				outputHtml = outputHtml.concat(galRespStr);
			}
			outputPane.setText(outputHtml);
			mainApp.importNetsAndNoasOutputFrame.setContentPane(
				outputScrollPane
			);
			mainApp.importNetsAndNoasOutputFrame.setVisible(true);
			mainApp.importNetsAndNoasOutputFrame.pack();
			mainApp.importNetsAndNoasOutputFrame.toFront();
		}
	}
}