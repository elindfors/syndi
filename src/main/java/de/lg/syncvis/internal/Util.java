package de.lg.syncvis.internal;

import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;

public class Util
{
	public static InputStream getResourceFile(String file) throws IOException
	{
		return Util.class.getClassLoader().getResourceAsStream(file);
	}
	
	public static String readFile(String file,Object ... args) throws IOException
	{
		String res = IOUtils.toString(Util.getResourceFile(file));
		if(args.length >= 1)
		  res = String.format(res,args);
		return res;
	}

}
