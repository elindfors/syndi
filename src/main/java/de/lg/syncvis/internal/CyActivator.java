package de.lg.syncvis.internal;

import java.io.File;
import java.util.Properties;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.cytoscape.app.swing.CySwingAppAdapter;
import org.cytoscape.application.events.CyShutdownEvent;
import org.cytoscape.application.events.CyShutdownListener;
import org.cytoscape.application.events.SetCurrentNetworkViewListener;
import org.cytoscape.application.swing.CytoPanelComponent;
import org.cytoscape.event.CyEventHelper;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNetworkManager;
import org.cytoscape.model.CyNetworkTableManager;
import org.cytoscape.model.events.RowsSetListener;
import org.cytoscape.service.util.AbstractCyActivator;
import org.osgi.framework.BundleContext;

/**
 * 
 * @last update: Mar 08, 2018
 */
public class CyActivator extends AbstractCyActivator {
	static Logger logger = LogManager.getLogger(CyActivator.class.getName());
	private BundleContext context;
	public MyCytoPanel visPanel;
	public void start(BundleContext context) throws Exception {
		try {
			logger.debug("SyncVis starting");
			this.context = context;
			
			visPanel = new MyCytoPanel(this);
			registerService(
				context,
				visPanel,
				CytoPanelComponent.class,
				new Properties()
			);
			registerService(
				context,
				new MyRowsSetListener(this,visPanel),
				RowsSetListener.class,
				new Properties()
			);
			
			MyGalCytoPanel galPanel = new MyGalCytoPanel(this);
			registerService(
				context,
				galPanel,
				CytoPanelComponent.class,
				new Properties()
			);	
			
			registerService(
	         context,
	         new SelectionRedirector(visPanel),
	         SetCurrentNetworkViewListener.class,
	         new Properties()
	       );			
			
			registerService(
				context,
				new CyShutdownListener() {
					public void handleEvent(CyShutdownEvent e) {					
						logger.debug("CyShutdownListener.handleEvent");
						
						/*
						 * We delete all files the user may have on his/her local
						 * computer.
						 */
						(new File(System.getProperty("user.dir") + "/tempInputGenes.txt")).delete();
						(new File(System.getProperty("user.dir") + "/tempInputExpr.txt")).delete();
						(new File(System.getProperty("user.dir") + "/tempCondClust.txt")).delete();					
						(new File(System.getProperty("user.dir") + "/plotGenes.py")).delete();
						
						(new File(System.getProperty("user.dir") + "/tempMemeGenes.txt")).delete();
						(new File(System.getProperty("user.dir") + "/tempMemeGenomeAnnotation.fasta")).delete();
						(new File(System.getProperty("user.dir") + "/runMeme.py")).delete();
						
						(new File(System.getProperty("user.dir") + "/tempFimoGenes.txt")).delete();
						(new File(System.getProperty("user.dir") + "/tempFimoGenomeAnnotation.fasta")).delete();
						(new File(System.getProperty("user.dir") + "/runFimo.py")).delete();
						
						// (new File(System.getProperty("user.dir") + "/tempFimoInputSeq.fasta")).delete();
						// (new File(System.getProperty("user.dir") + "/runFimo.py")).delete();
						
						(new File(System.getProperty("user.dir") + "/tempMeme2FimoGenes.txt")).delete();
						(new File(System.getProperty("user.dir") + "/tempMeme2FimoGenomeAnnotation.fasta")).delete();
						(new File(System.getProperty("user.dir") + "/runMeme2Fimo.py")).delete();
						
						(new File(System.getProperty("user.dir") + "/importNetsFromGal.py")).delete();
						(new File(System.getProperty("user.dir") + "/importMemeOutputsFromGal.py")).delete();
						(new File(System.getProperty("user.dir") + "/getGalToolPanel.py")).delete();
						(new File(System.getProperty("user.dir") + "/performDeAnal.py")).delete();
					}
				},
				CyShutdownListener.class,
				new Properties()
			);
		} catch (Throwable th) {
			logger.error(th);
			th.printStackTrace();
		}
	}
	public <S> S getService(Class<S> service) {
		return this.getService(context,service);
	}
	public CySwingAppAdapter getAdapter() {
		return this.getService(CySwingAppAdapter.class);
	}
	public CyNetworkManager getNetworkManager() {
		return this.getAdapter().getCyNetworkManager();
	}
	public CyEventHelper getEventHelper() {
	  return this.getAdapter().getCyEventHelper();
	}
	public Set<CyNetwork> getAllNetworks() {
		return this.getNetworkManager().getNetworkSet();
	}
	public CyNetworkTableManager getNetworkTableManager() {
		return this.getService(CyNetworkTableManager.class);
	}
}