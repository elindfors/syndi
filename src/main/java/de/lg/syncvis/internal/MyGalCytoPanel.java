package de.lg.syncvis.internal;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;
import java.util.Vector;
import java.util.Enumeration;

import javax.swing.AbstractButton;
import javax.swing.GroupLayout;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JEditorPane;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.ButtonGroup;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.tree.DefaultMutableTreeNode;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.cytoscape.application.swing.CytoPanelComponent;
import org.cytoscape.application.swing.CytoPanelName;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyNode;
import org.cytoscape.model.CyTableUtil;
import org.cytoscape.work.TaskIterator;
import org.cytoscape.work.swing.DialogTaskManager;

/**
 * 
 * 
 * @last update: Apr 05, 2018
 */
public class MyGalCytoPanel extends JPanel implements CytoPanelComponent {
	private static final long serialVersionUID = 947117508694091505L;
	
	final static String DEFAULT_GAL_URL = "http://localhost:8080";
	final static String DEFAULT_GAL_API_KEY = "";
	
	private static JTextField galUrlTf;
	private static JTextField galApiKeyTf;
	
	static String currentGalUrl = DEFAULT_GAL_URL;
	static String currentGalApiKey = DEFAULT_GAL_API_KEY;
	
	static Logger logger = LogManager.getLogger(MyCytoPanel.class.getName());
	private CyActivator master;
	
	Set<String> selPlotGenes = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
	private static JPanel expGenePanel;
	private static JEditorPane inputPlotGenesFramePane = new JEditorPane();
	private static CheckBoxNode plotGenesCbs[];
	private static File geFile;
	private static File condClustFile;
	
	private static JEditorPane inputMemeGenesPane = new JEditorPane();
	private static JEditorPane inputFimoGenesPane = new JEditorPane();
	private static JEditorPane inputMeme2FimoGenesPane = new JEditorPane();
	
	
	private LinkedList<GeneDirection> geneDirections = new LinkedList<GeneDirection>();
	
	HashMap<String, CyNetwork> selPlotGeneCyNets = new HashMap<String, CyNetwork>();
	
	Set<String> selMemeGenes = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
	HashMap<String, CyNetwork> selMemeGeneCyNets = new HashMap<String, CyNetwork>();
	static File memeGenomeAnnotFile;
	
	Set<String> selMeme2FimoGenes = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
	HashMap<String, CyNetwork> selMeme2FimoGeneCyNets = new HashMap<String, CyNetwork>();
	static File meme2FimoGenomeAnnotFile;
	
	Set<String> selFimoGenes = new TreeSet<String>(String.CASE_INSENSITIVE_ORDER);
	HashMap<String, CyNetwork> selFimoGeneCyNets = new HashMap<String, CyNetwork>();
	// static String fimoSeqString;
	static File fimoGenomeAnnotFile;
	
	String[] condClustStrings = {"Do you want to use condition clusters (yes,no)?","yes","no"};
	final JComboBox<String> condClustComboBox = new JComboBox<String>(condClustStrings);
	
	private static JPanel memeOutputPanel = new JPanel();
	final ButtonGroup memeOutputButtonGroup = new ButtonGroup();
	JFrame importNetsAndNoasOutputFrame = new JFrame();
	
	public MyGalCytoPanel(CyActivator master) {
		this.master = master;
		
		JButton addPlotGenesButton = new JButton("Add Selected Genes to Input");
		addPlotGenesButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addPlotGenes();
			}
		});
		
		JButton plotGenesButton = new JButton("Plot Genes");
		plotGenesButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				plotGenes();
			}
		});
		JPanel plotGenesPanel = new JPanel();
		GroupLayout plotGenesLayout = new GroupLayout(plotGenesPanel);
		plotGenesLayout.setAutoCreateGaps(true);
		plotGenesLayout.setAutoCreateContainerGaps(true);
		plotGenesLayout.setVerticalGroup(
			plotGenesLayout.createSequentialGroup().addGroup(
				plotGenesLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(addPlotGenesButton)
					.addComponent(plotGenesButton)
			)
		);
		plotGenesLayout.setHorizontalGroup(plotGenesLayout.createSequentialGroup()
	         .addComponent(addPlotGenesButton)
	         .addComponent(plotGenesButton)
		);
		plotGenesPanel.setLayout(plotGenesLayout);
		
		
		/*
		 * The following multiline JButton implementation is adopted from
		 * http://www.javaworld.com/article/2077368/learn-java/a-multiline-button-is-possible.html
		 */
		JButton importNetFromGalButton = new JButton();
		importNetFromGalButton.setLayout(new BorderLayout());
		JLabel label1 = new JLabel("Import Networks and Node Attributes From Galaxy ");
		JLabel label2 = new JLabel("Server (*.xgmml, *.noa)");
		importNetFromGalButton.add(BorderLayout.NORTH,label1);
		importNetFromGalButton.add(BorderLayout.SOUTH,label2);
		importNetFromGalButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.debug("importNetFromGalButton clicked");
				
				currentGalUrl = galUrlTf.getText().trim();
				currentGalApiKey = galApiKeyTf.getText().trim();
				if (currentGalUrl == null
						|| currentGalUrl.length() == 0
				) {
					JOptionPane.showMessageDialog(
						null,
						"You have not given the URL for Galaxy Server.\n" +
						"Please give it on 'Galaxy Server URL' field."
					);
					return;
				}
				if (currentGalApiKey == null
						|| currentGalApiKey.length() == 0
				) {
					JOptionPane.showMessageDialog(
						null,
						"You have not given the API key for Galaxy Server.\n" +
						"Please give it on 'Galaxy Server API KEY' field."
					);
					return;
				}
				DialogTaskManager dialogTaskManager =
					MyGalCytoPanel.this.master.getService(
						DialogTaskManager.class
					);
				dialogTaskManager.execute(new TaskIterator(
					new ImportNetsAndNoasFromGalMonitorTask(MyGalCytoPanel.this)
				));
			}
		});
		JPanel importNetFromGalPanel = new JPanel();
		GroupLayout importNetFromGalLayout = new GroupLayout(importNetFromGalPanel);
		importNetFromGalLayout.setAutoCreateGaps(true);
		importNetFromGalLayout.setAutoCreateContainerGaps(true);
		importNetFromGalLayout.setVerticalGroup(
			importNetFromGalLayout.createSequentialGroup().addGroup(
				importNetFromGalLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(importNetFromGalButton)
			)
		);
		importNetFromGalLayout.setHorizontalGroup(
			importNetFromGalLayout.createSequentialGroup()
				.addGroup(importNetFromGalLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
	        	.addComponent(importNetFromGalButton)
        	)
		);
		importNetFromGalPanel.setLayout(importNetFromGalLayout);
		
		JPanel galUrlPanel = new JPanel();
		JLabel galUrlLabel = new JLabel("Galaxy Server URL: ");
		galUrlTf = new JTextField(currentGalUrl);
		GroupLayout galUrlLayout = new GroupLayout(galUrlPanel);
		galUrlLayout.setAutoCreateGaps(true);
		galUrlLayout.setAutoCreateContainerGaps(true);
		galUrlLayout.setVerticalGroup(
			galUrlLayout.createSequentialGroup().addGroup(
				galUrlLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(galUrlLabel)
					.addComponent(galUrlTf)
			)
		);
		galUrlLayout.setHorizontalGroup(galUrlLayout.createSequentialGroup()
	         .addComponent(galUrlLabel)
	         .addComponent(galUrlTf)
		);
		galUrlPanel.setLayout(galUrlLayout);
		
		/*
		 * FIXME
		 * For some reason galApiKeyTf sometimes gets enlarged unreasonable wide
		 * The consequence is that apiKeyLabel will partially outside the visible
		 * canvas.
		 * I have tried setMaximumSize to fix this without success.
		 */
		JPanel apiKeyPanel = new JPanel();
		JLabel apiKeyLabel = new JLabel("Galaxy Server API KEY: ");
		galApiKeyTf = new JTextField(currentGalApiKey);
		// galApiKeyTf.setMaximumSize(new Dimension(1000,1000));
		GroupLayout apiKeyLayout = new GroupLayout(apiKeyPanel);
		apiKeyLayout.setAutoCreateGaps(true);
		apiKeyLayout.setAutoCreateContainerGaps(true);
		apiKeyLayout.setVerticalGroup(
			apiKeyLayout.createSequentialGroup().addGroup(
				apiKeyLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(apiKeyLabel)
					.addComponent(galApiKeyTf)
			)
		);
		apiKeyLayout.setHorizontalGroup(apiKeyLayout.createSequentialGroup()
	         .addComponent(apiKeyLabel)
	         .addComponent(galApiKeyTf)
		);
		apiKeyPanel.setLayout(apiKeyLayout);
		
		/*
		 * performDeAnal is much under development, so for the sake of simplicity
		 * it is ignored.
		 */
		/*
		performDeAnalButton = new JButton(
			"Perform Differential Gene Expression Analysis"
		);
		performDeAnalButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.debug("performDeAnalButton clicked");
				
				currentGalUrl = galUrlTf.getText().trim();
				currentGalApiKey = galApiKeyTf.getText().trim();
				if (currentGalUrl == null
						|| currentGalUrl.length() == 0
				) {
					JOptionPane.showMessageDialog(
						null,
						"You have not given the URL for Galaxy Server.\n" +
						"Please give it on 'Galaxy Server URL' field."
					);
					return;
				}
				if (currentGalApiKey == null
						|| currentGalApiKey.length() == 0
				) {
					JOptionPane.showMessageDialog(
						null,
						"You have not given the API key for Galaxy Server.\n" +
						"Please give it on 'Galaxy Server API KEY' field."
					);
					return;
				}
				DialogTaskManager dialogTaskManager =
					MyGalCytoPanel.this.master.getService(
						DialogTaskManager.class
					);
				dialogTaskManager.execute(new TaskIterator(
					new PerformDeAnalMonitorTask(MyGalCytoPanel.this)
				));
			}
		});
		JPanel performDeAnalPanel = new JPanel();
		GroupLayout performDeAnalLayout = new GroupLayout(performDeAnalPanel);
		performDeAnalLayout.setAutoCreateGaps(true);
		performDeAnalLayout.setAutoCreateContainerGaps(true);
		performDeAnalLayout.setVerticalGroup(
			performDeAnalLayout.createSequentialGroup().addGroup(
				performDeAnalLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(performDeAnalButton)
			)
		);
		performDeAnalLayout.setHorizontalGroup(
			performDeAnalLayout.createSequentialGroup()
				.addGroup(performDeAnalLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
	        	.addComponent(performDeAnalButton)
        	)
		);
		performDeAnalPanel.setLayout(performDeAnalLayout);
		*/
		
		JButton addMemeGenesButton = new JButton("Add Selected Genes to Input");
		addMemeGenesButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addGenesMeme();
			}
		});
		JButton runMemeButton = new JButton("Run MEME");
		runMemeButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				currentGalUrl = galUrlTf.getText().trim();
				currentGalApiKey = galApiKeyTf.getText().trim();
				if (currentGalUrl == null
						|| currentGalUrl.length() == 0
				) {
					JOptionPane.showMessageDialog(
						null,
						"You have not given the URL for Galaxy Server.\n" +
						"Please give it on 'Galaxy Server URL' field."
					);
					return;
				}
				if (currentGalApiKey == null
						|| currentGalApiKey.length() == 0
				) {
					JOptionPane.showMessageDialog(
						null,
						"You have not given the API key for Galaxy Server.\n" +
						"Please give it on 'Galaxy Server API KEY' field."
					);
					return;
				}
				runMeme();
			}
		});
		JPanel memePanel = new JPanel();
		GroupLayout memeLayout = new GroupLayout(memePanel);
		memeLayout.setAutoCreateGaps(true);
		memeLayout.setAutoCreateContainerGaps(true);
		memeLayout.setVerticalGroup(
			memeLayout.createSequentialGroup().addGroup(
				memeLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(addMemeGenesButton)
					.addComponent(runMemeButton)
			)
		);
		memeLayout.setHorizontalGroup(memeLayout.createSequentialGroup()
	         .addComponent(addMemeGenesButton)
	         .addComponent(runMemeButton)
		);
		memePanel.setLayout(memeLayout);
		
		JButton addFimoGenesButton = new JButton("Add Selected Genes to Input");
		addFimoGenesButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addGenesFimo();
			}
		});
		JButton runFimoButton = new JButton("Run FIMO");
		runFimoButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				runFimo();
			}
		});
		JPanel fimoPanel = new JPanel();
		GroupLayout fimoLayout = new GroupLayout(fimoPanel);
		fimoLayout.setAutoCreateGaps(true);
		fimoLayout.setAutoCreateContainerGaps(true);
		fimoLayout.setVerticalGroup(
			fimoLayout.createSequentialGroup().addGroup(
				fimoLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(addFimoGenesButton)
					.addComponent(runFimoButton)
			)
		);
		fimoLayout.setHorizontalGroup(fimoLayout.createSequentialGroup()
	         .addComponent(addFimoGenesButton)
	         .addComponent(runFimoButton)
		);
		fimoPanel.setLayout(fimoLayout);
		
		
		JButton addMeme2FimoGenesButton = new JButton("Add Selected Genes to Input");
		addMeme2FimoGenesButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addGenesMeme2Fimo();
			}
		});
		JButton runMeme2FimoButton = new JButton("Run Meme2Fimo");
		runMeme2FimoButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				currentGalUrl = galUrlTf.getText().trim();
				currentGalApiKey = galApiKeyTf.getText().trim();
				if (currentGalUrl == null
						|| currentGalUrl.length() == 0
				) {
					JOptionPane.showMessageDialog(
						null,
						"You have not given the URL for Galaxy Server.\n" +
						"Please give it on 'Galaxy Server URL' field."
					);
					return;
				}
				if (currentGalApiKey == null
						|| currentGalApiKey.length() == 0
				) {
					JOptionPane.showMessageDialog(
						null,
						"You have not given the API key for Galaxy Server.\n" +
						"Please give it on 'Galaxy Server API KEY' field."
					);
					return;
				}
				runMeme2Fimo();
			}
		});
		JPanel meme2FimoPanel = new JPanel();
		GroupLayout meme2FimoLayout = new GroupLayout(meme2FimoPanel);
		meme2FimoLayout.setAutoCreateGaps(true);
		meme2FimoLayout.setAutoCreateContainerGaps(true);
		meme2FimoLayout.setVerticalGroup(
			meme2FimoLayout.createSequentialGroup().addGroup(
				meme2FimoLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(addMeme2FimoGenesButton)
					.addComponent(runMeme2FimoButton)
			)
		);
		meme2FimoLayout.setHorizontalGroup(meme2FimoLayout.createSequentialGroup()
	         .addComponent(addMeme2FimoGenesButton)
	         .addComponent(runMeme2FimoButton)
		);
		meme2FimoPanel.setLayout(meme2FimoLayout);
		
	    JButton loadGeneDirections = new JButton("Load gene directions");
	    loadGeneDirections.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent e) {
	        loadGeneDirections();
	      }
	    });
	    JButton naiveOperonExtend = new JButton("Naive operon extend");
	    naiveOperonExtend.addActionListener(new ActionListener() {
	      public void actionPerformed(ActionEvent e) {
	        runNaiveExtend();
	      }
	    });
	    JPanel naiveExtendPanel = new JPanel();
	    GroupLayout naiveExtendLayout = new GroupLayout(naiveExtendPanel);
	    naiveExtendLayout.setAutoCreateGaps(true);
	    naiveExtendLayout.setAutoCreateContainerGaps(true);
	    naiveExtendLayout.setVerticalGroup(
	        naiveExtendLayout.createSequentialGroup().addGroup(
	            naiveExtendLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
	          .addComponent(loadGeneDirections)
	          .addComponent(naiveOperonExtend)
	      )
	    );
	    naiveExtendLayout.setHorizontalGroup(naiveExtendLayout.createSequentialGroup()
	           .addComponent(loadGeneDirections)
	           .addComponent(naiveOperonExtend)
	    );
	    naiveExtendPanel.setLayout(naiveExtendLayout);
		
    	/*
    	 * We have excluded a few panels (e.g. fimoPanel, memePanel) since they
    	 * do not work properly. However maybe we should not completely remove
    	 * them since they may work properly in the future.
    	 */
		JPanel wholePanel = new JPanel();
		GroupLayout wholeLayout = new GroupLayout(wholePanel);
		wholeLayout.setAutoCreateGaps(true);
		wholeLayout.setAutoCreateContainerGaps(true);
		wholeLayout.setHorizontalGroup(
			wholeLayout.createSequentialGroup().addGroup(
				wholeLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(galUrlPanel)
					.addComponent(apiKeyPanel)
				    // .addComponent(fimoPanel)
				    // .addComponent(memePanel)
				    .addComponent(meme2FimoPanel)
				    // .addComponent(naiveExtendPanel)
				    .addComponent(plotGenesPanel)
				    // .addComponent(performDeAnalPanel)
				    .addComponent(importNetFromGalPanel)
			)
		);
		wholeLayout.setVerticalGroup(wholeLayout.createSequentialGroup()
			.addComponent(galUrlPanel)
			.addComponent(apiKeyPanel)
			// .addComponent(fimoPanel)
            // .addComponent(memePanel)
            .addComponent(meme2FimoPanel)
            // .addComponent(naiveExtendPanel)
            .addComponent(plotGenesPanel)
            // .addComponent(performDeAnalPanel)
            .addComponent(importNetFromGalPanel)
		);
		wholePanel.setLayout(wholeLayout);
        this.add(wholePanel);
	}
	
	public void loadGeneDirections()
	{
    JFileChooser chooser = new JFileChooser();
    FileNameExtensionFilter filter = new FileNameExtensionFilter("*.tsv","tsv");
    chooser.addChoosableFileFilter(filter);
    int returnVal = chooser.showOpenDialog(null);
    if (returnVal == JFileChooser.APPROVE_OPTION)
    {
      try
      {
        String data = FileUtils.readFileToString(chooser.getSelectedFile());
        this.geneDirections.clear();
        for(String line : data.split("\n"))
        {
          String tmp[] = line.split("\t");
          this.geneDirections.add(new GeneDirection(tmp[0],tmp[1].trim().toUpperCase().equals("F")));
        }
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
    }
	  
	}
  public void runNaiveExtend()
  {
    HashSet<String> newSelNodes = this.master.visPanel.getSelectedNodeSharedNames();
    if(newSelNodes == null)
      return;
    String items[] = newSelNodes.toArray(new String[0]);
    for(String item : items)
    {
      for(int i = 0;i < this.geneDirections.size();i++)
      {
        GeneDirection dirItem = this.geneDirections.get(i);
        if(dirItem.geneId.equalsIgnoreCase(item))
        {
          for(int j = i;j < this.geneDirections.size() && j >= 0;)
          {
            GeneDirection newItem = this.geneDirections.get(j);
            if(dirItem.direction == newItem.direction)
            {
              newSelNodes.add(newItem.geneId);
              j += dirItem.direction ? 1 : -1;
            }
            else
              break;
          }
        }
      }
    }
    this.master.visPanel.setSelection(newSelNodes);
  }
	
	private void addGenesMeme() {
		logger.debug("addMemeGenesButton clicked");
		
		/*
		 * Retrieve the currently selected nodes from cyNets and add them to
		 * selMemeGenes and to selPlotGeneCyNets.
		 */
		Set<CyNetwork> cyNetSet = master.getAllNetworks();
		List<CyNetwork> cyNets = new ArrayList<CyNetwork>();
		cyNets.addAll(cyNetSet);
		
		Set<String> newSelMemeGenes = new HashSet<String>();
		for (int i = 0; i < cyNets.size(); i++) {
			CyNetwork cyNet = cyNets.get(i);
			List<CyNode> newSelNodes = CyTableUtil.getNodesInState(
				cyNet,"selected",true
			);
			for (int j = 0; j < newSelNodes.size(); j++) {
				CyNode newSelNode = newSelNodes.get(j);
				String newLabel = cyNet.getRow(newSelNode).get(
					"shared name",String.class
				);
				selMemeGenes.add(newLabel);
				selMemeGeneCyNets.put(newLabel,cyNet);
				newSelMemeGenes.add(newLabel);
			}
		}
		if (newSelMemeGenes.size() == 0) {
			JOptionPane.showMessageDialog(
				null,
				"You have not selected any node.\n" +
				"Please select at least one node if want to add gene(s) to input."
			);
			return;
		}
		if (newSelMemeGenes.size() <= 100) {
			List<String> newSelMemeGenesList = new ArrayList<String>();
			newSelMemeGenesList.addAll(newSelMemeGenes);
			String newSelNodesString = "";
			for (int i = 0; i < newSelMemeGenesList.size(); i++) {
				if (i > 0) {
					newSelNodesString = newSelNodesString.concat(", ");
				}
				newSelNodesString = newSelNodesString.concat(newSelMemeGenesList.get(i));
			}
			JOptionPane.showMessageDialog(
				null,
				"The following genes successfully added to input:\n "
				+ newSelNodesString
			);
		} else {
			JOptionPane.showMessageDialog(
				null,
				"The selected genes successfully added to input."
			);
		}  	
	  }
	  
	  private void runMeme() {
		logger.debug("runMemeButton clicked");
		JPanel inputMemePanel = new JPanel();	
		
		JScrollPane geneScrollPane = new JScrollPane(inputMemeGenesPane);
		inputMemeGenesPane.setEditable(false);
		inputMemeGenesPane.setContentType("text/html");
		
		/*
		 * We update the selMemeGenes on an html frame.
		 */
		if (selMemeGenes == null || selMemeGenes.size() == 0) {
			String html = "<html><body>Not any gene selected for input.</body></html>";
			inputMemeGenesPane.setText(html);
		} else {
			List<String> selMemeGenesList = new ArrayList<String>(selMemeGenes);
			String html = "<html><body><b>Input Genes:</b><br>";
			for (int i = 0; i < selMemeGenesList.size(); i++) {
				String selMemeGene = selMemeGenesList.get(i);
				if (i > 0) {
					html = html.concat("<br>");
				}
				html = html.concat(selMemeGene);
			}
			html = html.concat("</body></html>");
			inputMemeGenesPane.setText(html);
		}
		
		JButton runButton = new JButton("Run");
		try {
			runButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					currentGalUrl = galUrlTf.getText().trim();
					currentGalApiKey = galApiKeyTf.getText().trim();
					if (currentGalUrl == null
							|| currentGalUrl.length() == 0
					) {
						JOptionPane.showMessageDialog(
							null,
							"You have not given the URL for Galaxy Server.\n" +
							"Please give it on 'Galaxy Server URL' field."
						);
						return;
					}
					if (currentGalApiKey == null
							|| currentGalApiKey.length() == 0
					) {
						JOptionPane.showMessageDialog(
							null,
							"You have not given the API key for Galaxy Server.\n" +
							"Please give it on 'Galaxy Server API KEY' field."
						);
						return;
					}
					if (memeGenomeAnnotFile == null) {
					// if (memeGenomeAnnotStrg == null || memeGenomeAnnotStrg.length() == 0) {
						JOptionPane.showMessageDialog(
							null,
							"You have not uploaded genome annotation."
						);
						return;
					}
					
					/*
					 * TODO
					 * It might be reasonable to give here a heavy process
					 * warning message in the case there is not any input gene.
					 */
					/*
					 * if (selMemeGenes == null || selMemeGenes.size() == 0 ) {
					 *   JOptionPane.showMessageDialog(
					 *     null,
					 *     "You have not included any input gene."
					 *   );
					 *   return;
					 *}
					 */
					
					final DialogTaskManager dialogTaskManager =
						master.getService(DialogTaskManager.class);
					dialogTaskManager.execute(new TaskIterator(
						new RunMemeMonitorTask(MyGalCytoPanel.this)
					));
				}
			});
		}
		catch (Exception e1) {
			logger.error(e1);
		}
		
		JButton uploadMemeGenesButton = new JButton("Upload Input Genes");
		uploadMemeGenesButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.debug("uploadMemeGenesButton clicked");
				JFileChooser chooser = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter("*.txt","txt");
				chooser.addChoosableFileFilter(filter);
				int returnVal = chooser.showOpenDialog(null);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File geneFile = chooser.getSelectedFile();
					Scanner geneScanner = null;
					try {
						geneScanner = new Scanner(geneFile);
					} catch (Exception e1) {
						logger.error(e1);
					}
					geneScanner.nextLine();
					String genesString = "";
					boolean isFirstGene = true;
					while (geneScanner.hasNextLine()) {
						String row = geneScanner.nextLine();
						String[] cols = row.split("\t");
						String gene = cols[0].trim();
						selMemeGenes.add(gene);
						if (!isFirstGene) {
							genesString = genesString.concat(",");
						}
						genesString = genesString.concat(gene);
						isFirstGene = false;
					}
					geneScanner.close();
					
					/*
					 * We update the selMemeGenes on an html frame.
					 */
					if (selMemeGenes == null || selMemeGenes.size() == 0) {
						String html = "<html><body>Not any gene selected for input.</body></html>";
						inputMemeGenesPane.setText(html);
					} else {
						List<String> selMemeGenesList = new ArrayList<String>(selMemeGenes);
						String html = "<html><body><b>Input Genes:</b><br>";
						for (int i = 0; i < selMemeGenesList.size(); i++) {
							String selMemeGene = selMemeGenesList.get(i);
							if (i > 0) {
								html = html.concat("<br>");
							}
							html = html.concat(selMemeGene);
						}
						html = html.concat("</body></html>");
						inputMemeGenesPane.setText(html);
					}
					
					JOptionPane.showMessageDialog(
						null,
						"Uploading completed.\n" +
						"The following genes successfully added to input:\n" +
						genesString
					);
				}
			}
		});
		
		JButton emptyInputMemeGenesButton = new JButton("Empty Input Genes");
		emptyInputMemeGenesButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.debug("emptyInputMemeGenesButton clicked");
				selMemeGenes = new HashSet<String>();
				inputMemeGenesPane.setText(
					"<html><body>Not any gene selected for input.</body></html>"
				);
			}
		});
		
		JButton uploadGenomeAnnotButton = new JButton("Upload Genome Annotation");
		uploadGenomeAnnotButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.debug("uploadGenomeAnnotButton clicked");
				JFileChooser chooser = new JFileChooser();
				/*
				 * FIXME
				 * For some reason the following filter for gbk extension does
				 * not work.
				 */
				FileNameExtensionFilter filter =
					new FileNameExtensionFilter("*.gbk","GenBank");
				chooser.addChoosableFileFilter(filter);
				int returnVal = chooser.showOpenDialog(null);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					memeGenomeAnnotFile = chooser.getSelectedFile();
					JOptionPane.showMessageDialog(
						null,
						"Uploading completed."
					);
				}
			}
		});
		
		GroupLayout inputMemeLayout = new GroupLayout(inputMemePanel);
		inputMemeLayout.setAutoCreateGaps(true);
		inputMemeLayout.setAutoCreateContainerGaps(true);
		inputMemeLayout.setHorizontalGroup(
			inputMemeLayout.createSequentialGroup().addGroup(
				inputMemeLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
				 	.addComponent(geneScrollPane)
					.addComponent(uploadMemeGenesButton)
					.addComponent(emptyInputMemeGenesButton)
					.addComponent(uploadGenomeAnnotButton)
					.addComponent(runButton)
			)
		);
		inputMemeLayout.setVerticalGroup(inputMemeLayout.createSequentialGroup()
			.addComponent(geneScrollPane)
			.addComponent(uploadMemeGenesButton)
			.addComponent(emptyInputMemeGenesButton)
			.addComponent(uploadGenomeAnnotButton)
		    .addComponent(runButton)
		);
		inputMemePanel.setLayout(inputMemeLayout);
		
		final JFrame inputMemeFrame = new JFrame();
		// inputMemeFrame.setSize(200,350);
		inputMemeFrame.add(inputMemePanel);
		inputMemeFrame.setVisible(true);
		inputMemeFrame.pack();
	}
	
	private void addGenesMeme2Fimo() {
		logger.debug("addMeme2FimoGenesButton clicked");
		
		/*
		 * Retrieve the currently selected nodes from cyNets and add them to
		 * selMeme2FimoGenes and to selPlotGeneCyNets.
		 */
		Set<CyNetwork> cyNetSet = master.getAllNetworks();
		List<CyNetwork> cyNets = new ArrayList<CyNetwork>();
		cyNets.addAll(cyNetSet);
		
		Set<String> newSelMeme2FimoGenes = new HashSet<String>();
		for (int i = 0; i < cyNets.size(); i++) {
			CyNetwork cyNet = cyNets.get(i);
			List<CyNode> newSelNodes = CyTableUtil.getNodesInState(
				cyNet,"selected",true
			);
			for (int j = 0; j < newSelNodes.size(); j++) {
				CyNode newSelNode = newSelNodes.get(j);
				String newLabel = cyNet.getRow(newSelNode).get(
					"shared name",String.class
				);
				selMeme2FimoGenes.add(newLabel);
				selMeme2FimoGeneCyNets.put(newLabel,cyNet);
				newSelMeme2FimoGenes.add(newLabel);
			}
		}
		if (newSelMeme2FimoGenes.size() == 0) {
			JOptionPane.showMessageDialog(
				null,
				"You have not selected any node.\n" +
				"Please select at least one node if want to add gene(s) to input."
			);
			return;
		}
		if (newSelMeme2FimoGenes.size() <= 100) {
			List<String> newSelMeme2FimoGenesList = new ArrayList<String>();
			newSelMeme2FimoGenesList.addAll(newSelMeme2FimoGenes);
			String newSelNodesString = "";
			for (int i = 0; i < newSelMeme2FimoGenesList.size(); i++) {
				if (i > 0) {
					newSelNodesString = newSelNodesString.concat(", ");
				}
				newSelNodesString = newSelNodesString.concat(newSelMeme2FimoGenesList.get(i));
			}
			JOptionPane.showMessageDialog(
				null,
				"The following genes successfully added to input:\n "
				+ newSelNodesString
			);
		} else {
			JOptionPane.showMessageDialog(
				null,
				"The selected genes successfully added to input."
			);
		}  	
	  }
	
	private void runMeme2Fimo() {
		final JFrame geneFrame = new JFrame();
		geneFrame.setSize(200,350);
		
		JScrollPane inputMeme2FimoGenesScrollPane =
			new JScrollPane(inputMeme2FimoGenesPane);
		inputMeme2FimoGenesPane.setEditable(false);
		inputMeme2FimoGenesPane.setContentType("text/html");
		
		/*
		 * We update the selMeme2FimoGenes on an html frame.
		 */
		if (selMeme2FimoGenes == null || selMeme2FimoGenes.size() == 0) {
			String html = "<html><body>Not any gene selected for input.</body></html>";
			inputMeme2FimoGenesPane.setText(html);
		} else {
			List<String> selMeme2FimoGenesList = new ArrayList<String>(selMeme2FimoGenes);
			String html = "<html><body><b>Input Genes:</b><br>";
			for (int i = 0; i < selMeme2FimoGenesList.size(); i++) {
				String selMeme2FimoGene = selMeme2FimoGenesList.get(i);
				if (i > 0) {
					html = html.concat("<br>");
				}
				html = html.concat(selMeme2FimoGene);
			}
			html = html.concat("</body></html>");
			inputMeme2FimoGenesPane.setText(html);
		}
		
		JButton runButton = new JButton("Run");
		try {
			runButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					currentGalUrl = galUrlTf.getText().trim();
					currentGalApiKey = galApiKeyTf.getText().trim();
					if (currentGalUrl == null
							|| currentGalUrl.length() == 0
					) {
						JOptionPane.showMessageDialog(
							null,
							"You have not given the URL for Galaxy Server.\n" +
							"Please give it on 'Galaxy Server URL' field."
						);
						return;
					}
					if (currentGalApiKey == null
							|| currentGalApiKey.length() == 0
					) {
						JOptionPane.showMessageDialog(
							null,
							"You have not given the API key for Galaxy Server.\n" +
							"Please give it on 'Galaxy Server API KEY' field."
						);
						return;
					}
					if (meme2FimoGenomeAnnotFile == null) {
						JOptionPane.showMessageDialog(
							null,
							"You have not given the genome annotation file."
						);
						return;
					}
					
					/*
					 * TODO
					 * It might be reasonable to give here a heavy process
					 * warning message in the case there is not any input gene.
					 */
					/*
					 * if (selMeme2FimoGenes == null || selMeme2FimoGenes.size() == 0 ) {
					 *   JOptionPane.showMessageDialog(
					 * 	   null,
					 * 	   "You have not included any input gene."
					 *    );
					 *    return;
					 * }
					 */
					
					final DialogTaskManager dialogTaskManager =
						master.getService(DialogTaskManager.class);
					dialogTaskManager.execute(new TaskIterator(
						new RunMeme2FimoMonitorTask(MyGalCytoPanel.this)
					));
				}
			});
		}
		catch (Exception e1) {
			logger.error(e1);
		}
		// genePanel.add(geneScrollPane);
		
		JButton uploadMeme2FimoGenesButton = new JButton("Upload Input Genes");
		uploadMeme2FimoGenesButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.debug("uploadMeme2FimoGenesButton clicked");
				JFileChooser chooser = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter("*.txt","txt");
				chooser.addChoosableFileFilter(filter);
				int returnVal = chooser.showOpenDialog(null);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File geneFile = chooser.getSelectedFile();
					Scanner geneScanner = null;
					try {
						geneScanner = new Scanner(geneFile);
					} catch (Exception e1) {
						logger.error(e1);
					}
					geneScanner.nextLine();
					String genesString = "";
					boolean isFirstGene = true;
					while (geneScanner.hasNextLine()) {
						String row = geneScanner.nextLine();
						String[] cols = row.split("\t");
						String gene = cols[0].trim();
						selMeme2FimoGenes.add(gene);
						if (!isFirstGene) {
							genesString = genesString.concat(",");
						}
						genesString = genesString.concat(gene);
						isFirstGene = false;
					}
					geneScanner.close();
					
					/*
					 * We update the selMeme2FimoGenes in an html frame.
					 */
					if (selMeme2FimoGenes == null || selMeme2FimoGenes.size() == 0) {
						String html = "<html><body>Not any gene selected for input.</body></html>";
						inputMeme2FimoGenesPane.setText(html);
					} else {
						List<String> selMeme2FimoGenesList = new ArrayList<String>(selMeme2FimoGenes);
						String html = "<html><body><b>Input Genes:</b><br>";
						for (int i = 0; i < selMeme2FimoGenesList.size(); i++) {
							String selMeme2FimoGene = selMeme2FimoGenesList.get(i);
							if (i > 0) {
								html = html.concat("<br>");
							}
							html = html.concat(selMeme2FimoGene);
						}
						html = html.concat("</body></html>");
						inputMeme2FimoGenesPane.setText(html);
					}
					
					JOptionPane.showMessageDialog(
						null,
						"Uploading completed.\n" +
						"The following genes successfully added to input:\n" +
						genesString
					);
				}
			}
		});
		
		JButton emptyInputMeme2FimoGenesButton = new JButton("Empty Input Genes");
		emptyInputMeme2FimoGenesButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.debug("emptyInputMeme2FimoGenesButton clicked");
				selMeme2FimoGenes = new HashSet<String>();
				inputMeme2FimoGenesPane.setText("<html><body>Not any gene selected for input.</body></html>");
			}
		});
		

		JButton uploadGenomeAnnotButton = new JButton("Upload Genome Annotation");
		uploadGenomeAnnotButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.debug("uploadGenomeAnnotationButton clicked");
				JFileChooser chooser = new JFileChooser();
				/*
				 * FIXME
				 * For some reason the following filter for gbk extension does
				 * not work.
				 */
				FileNameExtensionFilter filter =
					new FileNameExtensionFilter("*.gbk","GenBank");
				chooser.addChoosableFileFilter(filter);
				int returnVal = chooser.showOpenDialog(null);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					meme2FimoGenomeAnnotFile = chooser.getSelectedFile();
					JOptionPane.showMessageDialog(
						null,
						"Uploading completed."
					);
				}
			}
		});
		
		JPanel meme2FimoPanel = new JPanel();
		GroupLayout inputFimoLayout = new GroupLayout(meme2FimoPanel);
		inputFimoLayout.setAutoCreateGaps(true);
		inputFimoLayout.setAutoCreateContainerGaps(true);
		inputFimoLayout.setHorizontalGroup(
			inputFimoLayout.createSequentialGroup().addGroup(
				inputFimoLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
				 	.addComponent(inputMeme2FimoGenesScrollPane)
					.addComponent(uploadMeme2FimoGenesButton)
					.addComponent(emptyInputMeme2FimoGenesButton)
					.addComponent(uploadGenomeAnnotButton)
					.addComponent(runButton)
			)
		);
		inputFimoLayout.setVerticalGroup(inputFimoLayout.createSequentialGroup()
			.addComponent(inputMeme2FimoGenesScrollPane)
			.addComponent(uploadMeme2FimoGenesButton)
			.addComponent(emptyInputMeme2FimoGenesButton)
			.addComponent(uploadGenomeAnnotButton)
		    .addComponent(runButton)
		);
		meme2FimoPanel.setLayout(inputFimoLayout);
		
		final JFrame meme2FimoFrame = new JFrame();
		meme2FimoFrame.add(meme2FimoPanel);
		meme2FimoFrame.setVisible(true);
		meme2FimoFrame.pack();
	}
	
	private void addPlotGenes() {
		logger.debug("addPlotGenesButton clicked");
		
		/*
		 * Retrieve the currently selected nodes from cyNets and add them to
		 * selPlotGenes and to selPlotGeneCyNets.
		 */
		Set<CyNetwork> cyNetSet = this.master.getAllNetworks();
		List<CyNetwork> cyNets = new ArrayList<CyNetwork>();
		cyNets.addAll(cyNetSet);
		
		Set<String> newSelPlotGenes = new HashSet<String>();
		for (int i = 0; i < cyNets.size(); i++) {
			CyNetwork cyNet = cyNets.get(i);
			List<CyNode> newSelNodes = CyTableUtil.getNodesInState(
				cyNet,"selected",true
			);
			for (int j = 0; j < newSelNodes.size(); j++) {
				CyNode newSelNode = newSelNodes.get(j);
				String newLabel = cyNet.getRow(newSelNode).get(
					"shared name",
					String.class
				);
				selPlotGenes.add(newLabel);
				selPlotGeneCyNets.put(newLabel,cyNet);
				newSelPlotGenes.add(newLabel);
			}
		}
		
		if (newSelPlotGenes.size() == 0) {
			JOptionPane.showMessageDialog(
				null,
				"You have not selected any node.\n" +
				"Please select at least one node if you want to add gene(s) " +
				"to input."
			);
			return;
		}
		
		if (newSelPlotGenes.size() <= 100) {
			List<String> newSelPlotGenesList = new ArrayList<String>();
			newSelPlotGenesList.addAll(newSelPlotGenes);
			String newSelNodesString = "";
			for (int i = 0; i < newSelPlotGenesList.size(); i++) {
				if (i > 0) {
					newSelNodesString = newSelNodesString.concat(", ");
				}
				newSelNodesString = newSelNodesString.concat(
					newSelPlotGenesList.get(i)
				);
			}
			JOptionPane.showMessageDialog(
				null,
				"The following genes successfully added to input:\n " +
				newSelNodesString
			);
		} else {
			JOptionPane.showMessageDialog(
				null,
				"The first 1000 selected genes successfully added to input."
			);
		}
	}
	
	private void plotGenes() {
		logger.debug("plotGenesButton clicked");
		
		currentGalUrl = galUrlTf.getText().trim();
		currentGalApiKey = galApiKeyTf.getText().trim();
		if (currentGalUrl == null
				|| currentGalUrl.length() == 0
		) {
			JOptionPane.showMessageDialog(
				null,
				"You have not given the URL for Galaxy Server.\n" +
				"Please give it on 'Galaxy Server URL' field."
			);
			return;
		}
		if (currentGalApiKey == null
				|| currentGalApiKey.length() == 0
		) {
			JOptionPane.showMessageDialog(
				null,
				"You have not given the API key for Galaxy Server.\n" +
				"Please give it on 'Galaxy Server API KEY' field."
			);
			return;
		}
		/*
		final JFrame plotGenesFrame = new JFrame();
		plotGenesFrame.setSize(200,350);
		*/
		
		// final JPanel plotGenesPanel = new JPanel(new GridLayout(6,1));
		JPanel plotGenesPanel = new JPanel();
		
		inputPlotGenesFramePane = new JEditorPane();
		JScrollPane inputPlotGenesFrameScrollPane = new JScrollPane(inputPlotGenesFramePane);
		inputPlotGenesFramePane.setEditable(false);
		inputPlotGenesFramePane.setContentType("text/html");
		
		String html = "<html><body><b>Input Genes:</b><br>";
		if (selPlotGenes == null || selPlotGenes.size() == 0) {
			html = "<html><body>Not any gene selected for input.</body></html>";
		} else {
			List<String> selPlotGenesList = new ArrayList<String>(selPlotGenes);
			
			for (int i = 0; i < selPlotGenesList.size(); i++) {
				String selPlotGene = selPlotGenesList.get(i);
				if (i > 0) {
					html = html.concat("<br>");
				}
				html = html.concat(selPlotGene);
			}
			html = html.concat("</body></html>");
		}
		inputPlotGenesFramePane.setText(html);
		
		JButton plotButton = new JButton("Plot");
		try {
			plotButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					logger.debug("plotButton clicked");
					
					currentGalUrl = galUrlTf.getText().trim();
					currentGalApiKey = galApiKeyTf.getText().trim();
					if (currentGalUrl == null
							|| currentGalUrl.length() == 0
					) {
						JOptionPane.showMessageDialog(
							null,
							"You have not given the URL for Galaxy Server.\n" +
							"Please give it on 'Galaxy Server URL' field."
						);
						return;
					}
					if (currentGalApiKey == null
							|| currentGalApiKey.length() == 0
					) {
						JOptionPane.showMessageDialog(
							null,
							"You have not given the API key for Galaxy Server.\n" +
							"Please give it on 'Galaxy Server API KEY' field."
						);
						return;
					}
					
					if (geFile == null) {
						JOptionPane.showMessageDialog(
							null,
							"You have not uploaded gene expression data.\n " +
							"Please click 'Upload Gene Expression Data' " +
							"button to do it."
						);
						return;
					}
					
					if (condClustComboBox.getSelectedIndex() == 0) {
						JOptionPane.showMessageDialog(
							null,
							"You have not selected if you use condition " +
							"cluster data."
						);
						return;
					}
					
					if (condClustComboBox.getSelectedIndex() == 1 && condClustFile == null) {
						JOptionPane.showMessageDialog(
							null,
							"You have not uploaded condition cluster data.\n " +
							"Please click 'Upload Condition Cluster Data' " +
							"button to do it."
						);
						return;
					}
					
					final DialogTaskManager dialogTaskManager =
						master.getService(DialogTaskManager.class);
					dialogTaskManager.execute(new TaskIterator(
						new PlotGenesMonitorTask(MyGalCytoPanel.this)
					));
				}
			});
		} catch (Exception e1) {
			logger.error(e1);
		}
		// plotGenesPanel.add(plotGenesFrameScrollPane);
		
		JPanel inputPlotGenesPanel = new JPanel();
		GroupLayout inputPlotGenesLayout = new GroupLayout(inputPlotGenesPanel);
		inputPlotGenesLayout.setAutoCreateGaps(true);
		inputPlotGenesLayout.setAutoCreateContainerGaps(true);
		inputPlotGenesLayout.setVerticalGroup(
			inputPlotGenesLayout.createSequentialGroup().addGroup(
				inputPlotGenesLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(inputPlotGenesFrameScrollPane)
			)
		);
		inputPlotGenesLayout.setHorizontalGroup(
			inputPlotGenesLayout.createSequentialGroup()
				.addGroup(inputPlotGenesLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
	        	.addComponent(inputPlotGenesFrameScrollPane)
        	)
		);
		inputPlotGenesPanel.setLayout(inputPlotGenesLayout);
		
		
		JButton emptyInputPlotGenesButton = new JButton("Empty Input Genes");
		emptyInputPlotGenesButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.debug("emptyInputPlotGenesButton clicked");
				
				selPlotGenes = new HashSet<String>();
				inputPlotGenesFramePane.setText(
					"<html><body>Not any gene selected for input.</body></html>"
				);
				
				/*
				 * Make the checkboxes empty if they exist.
				 */
				if (plotGenesCbs != null) {
					expGenePanel.removeAll();
					for (int i = 0; i < plotGenesCbs.length; i++) {
						CheckBoxNode cb = plotGenesCbs[i];
						cb.setSelected(false);
					}
					Vector expGenesVector = new NamedVector(
						"Expression genes (Input genes selected)",
						plotGenesCbs
					);
					Object rootNodes[] = {expGenesVector};
					Vector rootVector = new NamedVector("Root",rootNodes);
					JTree plotGenesTree = new JTree(rootVector);
					CheckBoxNodeRenderer renderer = new CheckBoxNodeRenderer();
					plotGenesTree.setCellRenderer(renderer);
					plotGenesTree.setCellEditor(new CheckBoxNodeEditor(plotGenesTree));
					plotGenesTree.setEditable(true);
					JScrollPane expGeneScrollPane = new JScrollPane(plotGenesTree);
					expGenePanel.add(expGeneScrollPane);
					
					plotGenesTree.addTreeSelectionListener(
						new javax.swing.event.TreeSelectionListener() {
							public void valueChanged(TreeSelectionEvent evt) {
								plotGenesTreeValueChanged(evt);
							}
						}
					);
					
					/*
					 * TODO
					 * Modify this implementation so that the plotGenesTree
					 * is automatically opened immediately after loading its
					 * content.
					 */
					/*
					 * I have been trying to use the following line but never
					 * managed to get the code compiled properly. It is adopted
					 * from http://stackoverflow.com/questions/11076961/how-to-
					 * automatically
					 * -open-child-node-after-insert-into-root-node-in-jtree.
					 */
					/*
					 * myTree.expandPath(e.getPath());
					 * myTree.setSelectionPath(e.getPath());
					 */
					/*
					 * plotGenesTree.expandPath(plotGenesTree.getPath());
					 * plotGenesTree.setSelectionPath(plotGenesTree.getPath());
					 */
					
					/*
					 * The following magic trick make the gene tree visible.
					 * It is adopted from
					 * http://stackoverflow.com/questions/12491580/adding
					 * -jpanel-onto-another-jpanel-after-button-click
					 */
					expGenePanel.validate();
					expGenePanel.repaint();
				}
			}
			
		});
		// plotGenesPanel.add(emptyInputPlotGenesButton);
		
		JPanel emptyInputPlotGenesPanel = new JPanel();
		GroupLayout emptyInputPlotGenesLayout = new GroupLayout(emptyInputPlotGenesPanel);
		emptyInputPlotGenesLayout.setAutoCreateGaps(true);
		emptyInputPlotGenesLayout.setAutoCreateContainerGaps(true);
		emptyInputPlotGenesLayout.setVerticalGroup(
			emptyInputPlotGenesLayout.createSequentialGroup().addGroup(
				emptyInputPlotGenesLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(emptyInputPlotGenesButton)
			)
		);
		emptyInputPlotGenesLayout.setHorizontalGroup(
			emptyInputPlotGenesLayout.createSequentialGroup()
				.addGroup(emptyInputPlotGenesLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
	        	.addComponent(emptyInputPlotGenesButton)
        	)
		);
		emptyInputPlotGenesPanel.setLayout(emptyInputPlotGenesLayout);
		
		JButton uploadGeDataButton = new JButton("Upload Gene Expression Data");
		uploadGeDataButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.debug("uploadGeDataButton clicked");
				
				JFileChooser chooser = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter("*.txt","txt");
				chooser.addChoosableFileFilter(filter);
				filter = new FileNameExtensionFilter("*.soft","soft");
				chooser.addChoosableFileFilter(filter);
				int returnVal = chooser.showOpenDialog(null);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					geFile = chooser.getSelectedFile();
					
					/*
					 * Next we create a new file and copy the content of the
					 * selected file to the new file.
					 * 
					 * The new file will be used as an input of plotGenes.py.
					 */
					try {
						createNewFileAndCopyContentOfAnotherFile(
							System.getProperty("user.dir") +
							"/tempInputExpr.txt",
							geFile.getPath()
						);
					} catch (Exception e1) {
						logger.error(e1);
					}
					
					/*
					 * The following implementation is adopted from
					 * http://www.java2s
					 * .com/Code/Java/Swing-JFC/CheckBoxNodeTreeSample.htm.
					 */
					Scanner geneScanner = null;
					try {
						geneScanner = new Scanner(geFile);
					} catch (Exception e1) {
						logger.error(e1);
					}
					geneScanner.nextLine();
					List<String> genes = new ArrayList<String>();
					while (geneScanner.hasNextLine()) {
						String row = geneScanner.nextLine();
						String[] cols = row.split("\t");
						genes.add(cols[0].trim());
					}
					geneScanner.close();
					plotGenesCbs = new CheckBoxNode[genes.size()];
					for (int i = 0; i < genes.size(); i++) {
						String gene = genes.get(i);
						boolean isSelGene = false;
						if (selPlotGenes != null 
								&& selPlotGenes.contains(gene)
						) {
							isSelGene = true;
						}
						plotGenesCbs[i] = new CheckBoxNode(gene,isSelGene);
					}
					
					Vector expGenesVector = new NamedVector(
						"Expression genes (Input genes selected)", plotGenesCbs
					);
					Object rootNodes[] = {expGenesVector};
					Vector rootVector = new NamedVector("Root",rootNodes);
					JTree plotGenesTree = new JTree(rootVector);
					CheckBoxNodeRenderer renderer = new CheckBoxNodeRenderer();
					plotGenesTree.setCellRenderer(renderer);
					plotGenesTree.setCellEditor(new CheckBoxNodeEditor(plotGenesTree));
					plotGenesTree.setEditable(true);
					JScrollPane expGeneScrollPane = new JScrollPane(plotGenesTree);
					expGeneScrollPane = new JScrollPane(plotGenesTree);
					expGenePanel.add(expGeneScrollPane);
					
					plotGenesTree.addTreeSelectionListener(
						new javax.swing.event.TreeSelectionListener() {
							public void valueChanged(TreeSelectionEvent evt) {
								plotGenesTreeValueChanged(evt);
							}
						}
					);
					
					/*
					 * TODO
					 * Modify this implementation so that the plotGenesTree
					 * is automatically opened immediately after loading its
					 * content.
					 */
					/*
					 * I have been trying to use the following line but never
					 * managed to get the code compiled properly. It is adopted
					 * from http://stackoverflow.com/questions/11076961/how-to-
					 * automatically
					 * -open-child-node-after-insert-into-root-node-in-jtree.
					 */
					/*
					 * myTree.expandPath(e.getPath());
					 * myTree.setSelectionPath(e.getPath());
					 */
					/*
					 * plotGenesTree.expandPath(plotGenesTree.getPath());
					 * plotGenesTree.setSelectionPath(plotGenesTree.getPath());
					 */
					
					/*
					 * The following magic trick make the gene tree visible.
					 * It is adopted from
					 * http://stackoverflow.com/questions/12491580/adding
					 * -jpanel-onto-another-jpanel-after-button-click
					 */
					expGenePanel.validate();
					expGenePanel.repaint();
					
					/*
					 * The following setVisible is an effort of getting the
					 * expGenePanel visible immediately in here (i.e. when the
					 * GE data is uploaded).
					 * 
					 * Actually may this is needed, instead the next command do
					 * the job.
					 */
					// expGenePanel.setVisible(true);
					
					/*
					 * It seems the following commands are also needed to make
					 * expGenePanel visible immediately in here (i.e. when the
					 * GE data is uploaded).
					 */
					plotGenesPanel.validate();
					plotGenesPanel.repaint();
					
					JOptionPane.showMessageDialog(
						null,
						"Uploading completed."
					);
				}
			}
		});
		// plotGenesPanel.add(uploadGeDataButton);
		
		JPanel uploadGeDataPanel = new JPanel();
		GroupLayout uploadGeDataLayout = new GroupLayout(uploadGeDataPanel);
		uploadGeDataLayout.setAutoCreateGaps(true);
		uploadGeDataLayout.setAutoCreateContainerGaps(true);
		uploadGeDataLayout.setVerticalGroup(
			uploadGeDataLayout.createSequentialGroup().addGroup(
				uploadGeDataLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(uploadGeDataButton)
			)
		);
		uploadGeDataLayout.setHorizontalGroup(
			uploadGeDataLayout.createSequentialGroup()
				.addGroup(uploadGeDataLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
	        	.addComponent(uploadGeDataButton)
        	)
		);
		uploadGeDataPanel.setLayout(uploadGeDataLayout);
		
		
		expGenePanel = new JPanel(new GridLayout(1,1));
		JPanel expGeneGroupPanel = new JPanel();
		GroupLayout expGeneLayout = new GroupLayout(expGeneGroupPanel);
		expGeneLayout.setAutoCreateGaps(true);
		expGeneLayout.setAutoCreateContainerGaps(true);
		expGeneLayout.setVerticalGroup(
			expGeneLayout.createSequentialGroup().addGroup(
				expGeneLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(expGenePanel)
			)
		);
		expGeneLayout.setHorizontalGroup(
			expGeneLayout.createSequentialGroup()
				.addGroup(expGeneLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
	        	.addComponent(expGenePanel)
        	)
		);
		expGeneGroupPanel.setLayout(expGeneLayout);
		// plotGenesPanel.add(expGenePanel);
		
		
		
		// GridLayout condClustPanelLayout = new GridLayout(1,2);
		// JPanel condClustPanel = new JPanel(condClustPanelLayout);
		// plotGenesPanel.add(condClustPanel);
		
		final JButton uploadCondClustButton = new JButton("Upload Condition Cluster Data");
		
		condClustComboBox.setSelectedIndex(0);
		condClustComboBox.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e)
			{
				logger.debug("condClustComboBox clicked");
				if (condClustComboBox.getSelectedIndex() == 1) {
					/* "yes" is selected. */
					uploadCondClustButton.setVisible(true);
				} else if (condClustComboBox.getSelectedIndex() == 2) {
					/* "no" is selected. */
					uploadCondClustButton.setVisible(false);
				}
			}
		});
		// condClustPanel.add(condClustComboBox);
		
		uploadCondClustButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter("*.txt","txt");
				chooser.addChoosableFileFilter(filter);
				int returnVal = chooser.showOpenDialog(null);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					condClustFile = chooser.getSelectedFile();
					
					/*
					 * Next we create a new file and copy the content of the
					 * selected file to the new file.
					 * 
					 * The new file will be used as an input of plotGenes.py.
					 */
					try {
						createNewFileAndCopyContentOfAnotherFile(
							System.getProperty("user.dir") +
							"/tempCondClust.txt",
							condClustFile.getPath()
						);
						
						JOptionPane.showMessageDialog(
							null,
							"Uploading completed."
						);
					} catch (Exception e1) {
						logger.error(e1);
					}
				}
			}
		});
		uploadCondClustButton.setVisible(false);
		// condClustPanel.add(uploadCondClustButton);
		
		/*
		JPanel condClustPanel = new JPanel();
		GroupLayout condClustLayout = new GroupLayout(condClustPanel);
		condClustLayout.setAutoCreateGaps(true);
		condClustLayout.setAutoCreateContainerGaps(true);
		condClustLayout.setVerticalGroup(
			condClustLayout.createSequentialGroup().addGroup(
				condClustLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(condClustComboBox)
					.addComponent(uploadCondClustButton)
			)
		);
		condClustLayout.setHorizontalGroup(condClustLayout.createSequentialGroup()
	         .addComponent(condClustComboBox)
	         .addComponent(uploadCondClustButton)
		);
		condClustPanel.setLayout(condClustLayout);
		*/
		
		JPanel condClustPanel = new JPanel(new GridLayout(1,2));
		condClustPanel.add(condClustComboBox);
		condClustPanel.add(uploadCondClustButton);
		
		// plotGenesPanel.add(plotButton);
		
		JPanel plotButtonPanel = new JPanel();
		GroupLayout plotButtonLayout = new GroupLayout(plotButtonPanel);
		plotButtonLayout.setAutoCreateGaps(true);
		plotButtonLayout.setAutoCreateContainerGaps(true);
		plotButtonLayout.setVerticalGroup(
			plotButtonLayout.createSequentialGroup().addGroup(
				plotButtonLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
					.addComponent(plotButton)
			)
		);
		plotButtonLayout.setHorizontalGroup(
			plotButtonLayout.createSequentialGroup()
				.addGroup(plotButtonLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
	        	.addComponent(plotButton)
        	)
		);
		plotButtonPanel.setLayout(plotButtonLayout);
		
		// JPanel plotGenesPanel = new JPanel();
		GroupLayout plotGenesLayout = new GroupLayout(plotGenesPanel);
		plotGenesLayout.setAutoCreateGaps(true);
		plotGenesLayout.setAutoCreateContainerGaps(true);
		plotGenesLayout.setHorizontalGroup(
			plotGenesLayout.createSequentialGroup().addGroup(
				plotGenesLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
				 	.addComponent(inputPlotGenesPanel)
					.addComponent(emptyInputPlotGenesPanel)
					.addComponent(uploadGeDataPanel)
					.addComponent(expGeneGroupPanel)
					.addComponent(condClustPanel)
					.addComponent(plotButtonPanel)
			)
		);
		plotGenesLayout.setVerticalGroup(plotGenesLayout.createSequentialGroup()
				.addComponent(inputPlotGenesPanel)
				.addComponent(emptyInputPlotGenesPanel)
				.addComponent(uploadGeDataPanel)
				.addComponent(expGeneGroupPanel)
				.addComponent(condClustPanel)
			    .addComponent(plotButtonPanel)
		);
		plotGenesPanel.setLayout(plotGenesLayout);
		
		/*
		 * Maybe the following commands in order to get the expGenePanel visible
		 * immediately when the GE data is uploaded. 
		 */
		plotGenesPanel.validate();
		plotGenesPanel.repaint();
		
        this.add(plotGenesPanel);
		
        final JFrame plotGenesFrame = new JFrame();
		// plotGenesFrame.setSize(200,350);
		plotGenesFrame.add(plotGenesPanel);
		plotGenesFrame.setVisible(true);
		plotGenesFrame.pack();
		
		/*
		 * OBS!
		 * I think we should not use the geneFrame.toFront() at this point since
		 * I think there would be a risk that the galaxy results pop-up window
		 * would be hidden behind the geneFrame.
		 */
		// geneFrame.toFront();		
	}
	
	public void plotGenesTreeValueChanged(TreeSelectionEvent tse) {	
		/*
		 * The following lines are from
		 * http://www.java2s.com/Code/Java/Swing-JFC/CheckBoxNodeTreeSample.htm
		 */
		Object node = tse.getNewLeadSelectionPath().getLastPathComponent();
		DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) node;
		Object userObject = treeNode.getUserObject();
		if (!(userObject instanceof CheckBoxNode)) {
			return;
		}
		
		CheckBoxNode cbNode = (CheckBoxNode) userObject;
		String selGene = cbNode.getText().trim();
		
		/*
		 * We add/remove the selected nodes in/from selPlotGenes.
		 */
		if (!cbNode.isSelected()) {
			selPlotGenes.add(selGene);
		} else {
			selPlotGenes.remove(selGene);
		}
		
		/*
		 * We update the selPlotGenes on an html frame.
		 */
		if (selPlotGenes == null || selPlotGenes.size() == 0) {
			String html =
				"<html><body>Not any gene selected for input.</body></html>";
			inputPlotGenesFramePane.setText(html);
		} else {
			List<String> selPlotGenesList = new ArrayList<String>(selPlotGenes);
			String html = "<html><body><b>Input Genes:</b><br>";
			for (int i = 0; i < selPlotGenesList.size(); i++) {
				String selPlotGene = selPlotGenesList.get(i);
				if (i > 0) {
					html = html.concat("<br>");
				}
				html = html.concat(selPlotGene);
			}
			html = html.concat("</body></html>");
			inputPlotGenesFramePane.setText(html);
		}
		
		/*
		 * We toggle the selected node in the checkboxes.
		 */
		for (int i = 0; i < plotGenesCbs.length; i++) {
			CheckBoxNode cb = plotGenesCbs[i];
			if (cb.getText().equals(selGene)) {
				if (cb.selected) {
					cb.setSelected(false);
				} else {
					cb.setSelected(true);
				}
			}
		}
		
		/*
		 * We update plotGenesCbs in expGenePanel.
		 * 
		 * First we empty expGenePanel, so there will not appear multiple
		 * plotGenesTrees.
		 */
		expGenePanel.removeAll();
		
		Vector expGenesVector = new NamedVector(
			"Expression genes (Input genes selected)",
			plotGenesCbs
		);
		Object rootNodes[] = {expGenesVector};
		Vector rootVector = new NamedVector("Root",rootNodes);
		JTree plotGenesTree = new JTree(rootVector);
		CheckBoxNodeRenderer renderer = new CheckBoxNodeRenderer();
		plotGenesTree.setCellRenderer(renderer);
		plotGenesTree.setCellEditor(new CheckBoxNodeEditor(plotGenesTree));
		plotGenesTree.setEditable(true);
		JScrollPane expGeneScrollPane = new JScrollPane(plotGenesTree);
		expGenePanel.add(expGeneScrollPane);
		
		/*
		 * The following lines make the automatically plotGenesTree open. It is
		 * adopted from
		 * http://stackoverflow.com/questions/11076961/how-to-automatically
		 * -open-child-node-after-insert-into-root-node-in-jtree.
		 */
		plotGenesTree.expandPath(tse.getPath());
		plotGenesTree.setSelectionPath(tse.getPath());
		plotGenesTree.addTreeSelectionListener(
				new javax.swing.event.TreeSelectionListener()
		{
			public void valueChanged(TreeSelectionEvent evt) {
				plotGenesTreeValueChanged(evt);
			}
		});
		
		/*
		 * The following magic trick make the gene tree visible. It is adopted from
		 * http://stackoverflow.com/questions/12491580/adding-jpanel-onto-another-jpanel-after-button-click
		 */
		expGenePanel.validate();
		expGenePanel.repaint();
	}
	
	private void addGenesFimo() {
		logger.debug("addFimoGenesButton clicked");
		
		/*
		 * Retrieve the currently selected nodes from cyNets and add them to
		 * selFimoGenes and to selPlotGeneCyNets.
		 */
		Set<CyNetwork> cyNetSet = master.getAllNetworks();
		List<CyNetwork> cyNets = new ArrayList<CyNetwork>();
		cyNets.addAll(cyNetSet);
		
		Set<String> newSelFimoGenes = new HashSet<String>();
		for (int i = 0; i < cyNets.size(); i++) {
			CyNetwork cyNet = cyNets.get(i);
			List<CyNode> newSelNodes = CyTableUtil.getNodesInState(
				cyNet,"selected",true
			);
			for (int j = 0; j < newSelNodes.size(); j++) {
				CyNode newSelNode = newSelNodes.get(j);
				String newLabel = cyNet.getRow(newSelNode).get(
					"shared name",String.class
				);
				selFimoGenes.add(newLabel);
				selFimoGeneCyNets.put(newLabel,cyNet);
				newSelFimoGenes.add(newLabel);
			}
		}
		if (newSelFimoGenes.size() == 0) {
			JOptionPane.showMessageDialog(
				null,
				"You have not selected any node.\n" +
				"Please select at least one node if want to add gene(s) to " +
				"input."
			);
			return;
		}
		if (newSelFimoGenes.size() <= 100) {
			List<String> newSelFimoGenesList = new ArrayList<String>();
			newSelFimoGenesList.addAll(newSelFimoGenes);
			String newSelNodesString = "";
			for (int i = 0; i < newSelFimoGenesList.size(); i++) {
				if (i > 0) {
					newSelNodesString = newSelNodesString.concat(", ");
				}
				newSelNodesString = newSelNodesString.concat(
					newSelFimoGenesList.get(i)
				);
			}
			JOptionPane.showMessageDialog(
				null,
				"The following genes successfully added to input:\n " +
				newSelNodesString
			);
		} else {
			JOptionPane.showMessageDialog(
				null,
				"The selected genes successfully added to input."
			);
		}
	}
	
	public void runFimo() {
		logger.debug("runFimoButton clicked");
		
		currentGalUrl = galUrlTf.getText().trim();
		currentGalApiKey = galApiKeyTf.getText().trim();
		if (currentGalUrl == null
				|| currentGalUrl.length() == 0
		) {
			JOptionPane.showMessageDialog(
				null,
				"You have not given the URL for Galaxy Server.\n" +
				"Please give it on 'Galaxy Server URL' field."
			);
			return;
		}
		if (currentGalApiKey == null
				|| currentGalApiKey.length() == 0
		) {
			JOptionPane.showMessageDialog(
				null,
				"You have not given the API key for Galaxy Server.\n" +
				"Please give it on 'Galaxy Server API KEY' field."
			);
			return;
		}
		
		JScrollPane inputFimoGenesScrollPane =
			new JScrollPane(inputFimoGenesPane);
		inputFimoGenesPane.setEditable(false);
		inputFimoGenesPane.setContentType("text/html");
		
		/*
		 * We update the selFimoGenes on an html frame.
		 */
		if (selFimoGenes == null || selFimoGenes.size() == 0) {
			String html = "<html><body>Not any gene selected for input.</body></html>";
			inputFimoGenesPane.setText(html);
		} else {
			List<String> selFimoGenesList = new ArrayList<String>(selFimoGenes);
			String html = "<html><body><b>Input Genes:</b><br>";
			for (int i = 0; i < selFimoGenesList.size(); i++) {
				String selFimoGene = selFimoGenesList.get(i);
				if (i > 0) {
					html = html.concat("<br>");
				}
				html = html.concat(selFimoGene);
			}
			html = html.concat("</body></html>");
			inputFimoGenesPane.setText(html);
		}
		
		JButton runButton = new JButton("Run");
		try {
			runButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					currentGalUrl = galUrlTf.getText().trim();
					currentGalApiKey = galApiKeyTf.getText().trim();
					if (currentGalUrl == null
							|| currentGalUrl.length() == 0
					) {
						JOptionPane.showMessageDialog(
							null,
							"You have not given the URL for Galaxy Server.\n" +
							"Please give it on 'Galaxy Server URL' field."
						);
						return;
					}			
					if (currentGalApiKey == null
							|| currentGalApiKey.length() == 0
					) {
						JOptionPane.showMessageDialog(
							null,
							"You have not given the API key for Galaxy Server.\n" +
							"Please give it on 'Galaxy Server API KEY' field."
						);
						return;
					}
					if (fimoGenomeAnnotFile == null) {
					// if (fimoSeqString == null || fimoSeqString.length() == 0) {
						JOptionPane.showMessageDialog(
							null,
							"You have not uploaded genome annotation."
						);
						return;
					}
					
					/*
					 * The for-loop is adopted from
					 * https://stackoverflow.com/questions/201287/how-do-i-get-which-jradiobutton-is-selected-from-a-buttongroup
					 */
					boolean isAtLeastOneMemeOutputCbSelected = false;
					for (
						Enumeration<AbstractButton> buttons =
							memeOutputButtonGroup.getElements();
						buttons.hasMoreElements();
					) {
			            AbstractButton button = buttons.nextElement();
			            if (button.isSelected()) {
			            	isAtLeastOneMemeOutputCbSelected = true;
							break;
			            }
			        }
					if (!isAtLeastOneMemeOutputCbSelected) {
						JOptionPane.showMessageDialog(
							null,
							"You have not selected MEME output file."
						);
						return;
					}
					
					/*
					 * TODO
					 * It might be reasonable to give here a heavy process
					 * warning message in the case there is not any input gene.
					 */
					/*
					 * if (selFimoGenes == null || selFimoGenes.size() == 0 ) {
				     * 	JOptionPane.showMessageDialog(
					 * 		null,
					 * 		"You have not included any input gene."
					 * 	);
					 * 	return;
					 * }
					 */
					
					final DialogTaskManager dialogTaskManager =
						master.getService(DialogTaskManager.class);
					dialogTaskManager.execute(new TaskIterator(
						new RunFimoMonitorTask(MyGalCytoPanel.this)
					));
				}
			});
		} catch (Exception e1) {
			logger.error(e1);
		}
		
		JButton uploadFimoGenesButton = new JButton("Upload Input Genes");
		uploadFimoGenesButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.debug("uploadFimoGenesButton clicked");
				
				JFileChooser chooser = new JFileChooser();
				FileNameExtensionFilter filter = new FileNameExtensionFilter(
					"*.txt","txt"
				);
				chooser.addChoosableFileFilter(filter);
				int returnVal = chooser.showOpenDialog(null);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					File geneFile = chooser.getSelectedFile();
					Scanner geneScanner = null;
					try {
						geneScanner = new Scanner(geneFile);
					} catch (Exception e1) {
						logger.error(e1);
					}
					geneScanner.nextLine();
					String genesString = "";
					boolean isFirstGene = true;
					while (geneScanner.hasNextLine()) {
						String row = geneScanner.nextLine();
						String[] cols = row.split("\t");
						String gene = cols[0].trim();
						selFimoGenes.add(gene);
						if (!isFirstGene) {
							genesString = genesString.concat(",");
						}
						genesString = genesString.concat(gene);
						isFirstGene = false;
					}
					geneScanner.close();
					
					/*
					 * We update the selFimoGenes on an html frame.
					 */
					if (selFimoGenes == null || selFimoGenes.size() == 0) {
						String html =
							"<html><body>Not any gene selected for input.</body></html>";
						inputFimoGenesPane.setText(html);
					} else {
						List<String> selFimoGenesList =
							new ArrayList<String>(selFimoGenes);
						String html = "<html><body><b>Input Genes:</b><br>";
						for (int i = 0; i < selFimoGenesList.size(); i++) {
							String selFimoGene = selFimoGenesList.get(i);
							if (i > 0) {
								html = html.concat("<br>");
							}
							html = html.concat(selFimoGene);
						}
						html = html.concat("</body></html>");
						inputFimoGenesPane.setText(html);
					}
					
					JOptionPane.showMessageDialog(
						null,
						"Uploading completed.\n" +
						"The following genes successfully added to input:\n " + 
						genesString
					);
				}
			}
		});
		
		JButton emptyInputFimoGenesButton = new JButton("Empty Input Genes");
		emptyInputFimoGenesButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.debug("emptyInputFimoGenesButton clicked");
				selFimoGenes = new HashSet<String>();
				inputFimoGenesPane.setText(
					"<html><body>Not any gene selected for input.</body></html>"
				);
			}
		});
		
		
		JButton uploadGenomeAnnotButton = new JButton("Upload Genome Annotation");
		uploadGenomeAnnotButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				logger.debug("uploadGenomeAnnotButton clicked");
				JFileChooser chooser = new JFileChooser();
				/*
				 * FIXME
				 * For some reason the following filter for gbk extension does
				 * not work.
				 */
				FileNameExtensionFilter filter =
					new FileNameExtensionFilter("*.gbk","GenBank");
				chooser.addChoosableFileFilter(filter);
				int returnVal = chooser.showOpenDialog(null);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					fimoGenomeAnnotFile = chooser.getSelectedFile();
					JOptionPane.showMessageDialog(
						null,
						"Uploading completed."
					);
				}
			}
		});
		
		JPanel fimoPanel = new JPanel();
		final DialogTaskManager dialogTaskManager = master.getService(
			DialogTaskManager.class
		);
		final JFrame fimoFrame = new JFrame();
		dialogTaskManager.execute(new TaskIterator(
			new ImportMemeOutputsMonitorTask(
				MyGalCytoPanel.this,
				memeOutputButtonGroup,
				memeOutputPanel,
				fimoFrame
			)
		));
				
		GroupLayout inputFimoLayout = new GroupLayout(fimoPanel);
		inputFimoLayout.setAutoCreateGaps(true);
		inputFimoLayout.setAutoCreateContainerGaps(true);
		inputFimoLayout.setHorizontalGroup(
			inputFimoLayout.createSequentialGroup().addGroup(
				inputFimoLayout.createParallelGroup(GroupLayout.Alignment.CENTER)
				 	.addComponent(inputFimoGenesScrollPane)
					.addComponent(uploadFimoGenesButton)
					.addComponent(emptyInputFimoGenesButton)
					.addComponent(uploadGenomeAnnotButton)
					.addComponent(memeOutputPanel)
					.addComponent(runButton)
			)
		);
		inputFimoLayout.setVerticalGroup(inputFimoLayout.createSequentialGroup()
			.addComponent(inputFimoGenesScrollPane)
			.addComponent(uploadFimoGenesButton)
			.addComponent(emptyInputFimoGenesButton)
			.addComponent(uploadGenomeAnnotButton)
			.addComponent(memeOutputPanel)
		    .addComponent(runButton)
		);
		fimoPanel.setLayout(inputFimoLayout);
		
		fimoPanel.add(runButton);
		fimoFrame.add(fimoPanel);
		fimoFrame.setVisible(true);
		fimoFrame.pack();
	}
	
	/*
	 * This implementation is adopted from
	 * http://stackoverflow.com/questions/19598377
	 * /how-to-create-an-file-and-copy-the
	 * -content-from-another-file-into-the-created-fi?lq=1
	 */
	private static File createNewFileAndCopyContentOfAnotherFile(
		String newFilePath,
		String anotherFilePath
	) throws Exception {	
		File anotherFile = new File(anotherFilePath);
		File newFile = new File(newFilePath);
		InputStream is = new FileInputStream(anotherFile);
		OutputStream os = new FileOutputStream(newFile);
		byte[] buffer = new byte[4096];
		int read = 0;
		while ((read = is.read(buffer)) != -1) {
			os.write(buffer,0,read);
		}
		is.close();
		os.close();
		return newFile;
	}
	
	public Component getComponent() {
		return this;
	}
	
	public CytoPanelName getCytoPanelName() {
		return CytoPanelName.WEST;
	}
	
	public String getTitle() {
		return "SyncVis - Gal";
	}
	
	public Icon getIcon() {
		return null;
	}
}