package de.lg.syncvis.internal;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.cytoscape.model.CyNetwork;
import org.cytoscape.model.CyTable;
import org.cytoscape.model.events.RowsSetEvent;
import org.cytoscape.model.events.RowsSetListener;

/**
 * 
 * 
 * @last update: Mar 08, 2018
 */
public class MyRowsSetListener implements RowsSetListener {
	static Logger logger = LogManager.getLogger(MyRowsSetListener.class.getName());
	CyActivator master;
	MyCytoPanel mainApp;
	public MyRowsSetListener(CyActivator master,MyCytoPanel mainApp) {
		this.master = master;
		this.mainApp = mainApp;
	}
	
	/*
	 * Check if the event of interest a new node selection caused by an action
	 * and not something else.
	 */
	 public void handleEvent(RowsSetEvent e) {
		try {
  		/*
  		 * If node selection or removal is processing, then we immediately quit
  		 * the method since I think usually it would lead to a flow of this
  		 * method calls.
  		 */
		  logger.debug("Row change event in: " + mainApp.ignoreSelectionEvents);
  		if (mainApp.ignoreSelectionEvents != 0) {
  			return;
  		}
  		  		
  		CyTable table = e.getSource();
      
      /*
       * Get the current selected network and check if event come from the
       * currently selected node table.
       */
      CyNetwork selCyNet = null;
      for (CyNetwork cyNet : master.getAllNetworks()) {
        if (cyNet.getRow(cyNet).get(CyNetwork.SELECTED,Boolean.class) == true) {
          selCyNet = cyNet;
          break;
        }
      }
      
      if (selCyNet == null || selCyNet.getDefaultNodeTable() != table) {
        return;
      }
        	    
      /*
       * We are only interested in selection modifying events.
       */
      if (!e.containsColumn(CyNetwork.SELECTED)) {
        return;
      }
  	  logger.debug("Accepted row change event");
  	  mainApp.updateSelection();
  	  
    } catch(Throwable th) {
		  logger.debug(th);
		  th.printStackTrace();
		  throw th;
		}
	}
}