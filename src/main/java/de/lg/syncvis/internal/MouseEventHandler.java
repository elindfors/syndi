package de.lg.syncvis.internal;

import java.awt.Component;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import org.cytoscape.view.model.CyNetworkView;

public class MouseEventHandler implements MouseListener, KeyListener
{
  private MouseListener client;
  private MyCytoPanel mainApp;
  private CyNetworkView view;
  
  MouseEventHandler(Component comp, CyNetworkView view,MyCytoPanel mainApp)
  {
    this.client = (MouseListener)comp;
    this.mainApp = mainApp;
    this.view = view;
  }
  
  @Override
  public void mouseClicked(MouseEvent e)
  {
    //ignored anyhow
    client.mouseClicked(e);    
  }

  @Override
  public void mousePressed(MouseEvent e)
  {
    MyCytoPanel.logger.debug("mouse pressed");
    mainApp.ignoreSelectionEvents++;    
    client.mousePressed(e);     
    mainApp.ignoreSelectionEvents--;
  }

  @Override
  public void mouseReleased(MouseEvent e)
  {
    MyCytoPanel.logger.debug("mouse released");
    mainApp.ignoreSelectionEvents++;
    client.mouseReleased(e);   
    mainApp.ignoreSelectionEvents--;
    mainApp.updateSelection();
  }

  @Override
  public void mouseEntered(MouseEvent e)
  {
    //ignored anyhow
    client.mouseEntered(e);    
  }

  @Override
  public void mouseExited(MouseEvent e)
  {
    //ignored anyhow
    client.mouseExited(e);    
  }

  @Override
  public void keyTyped(KeyEvent e)
  {
    // TODO Auto-generated method stub
    
  }

  @Override
  public void keyPressed(KeyEvent e)
  {
    if(e.getKeyChar() == 'q')
      this.mainApp.qPressed = true;
    else if(e.getKeyChar() == 'w')
      this.mainApp.wPressed = true; 
  }

  @Override
  public void keyReleased(KeyEvent e)
  {
    if(e.getKeyChar() == 'q')
      this.mainApp.qPressed = false;
    else if(e.getKeyChar() == 'w')
      this.mainApp.wPressed = false;    
  }
  
}
