package de.lg.syncvis.internal;

import java.awt.Component;
import java.awt.event.MouseListener;
import java.lang.reflect.Method;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.cytoscape.application.events.SetCurrentNetworkViewEvent;
import org.cytoscape.application.events.SetCurrentNetworkViewListener;
import org.cytoscape.view.model.CyNetworkView;
 
public class SelectionRedirector implements SetCurrentNetworkViewListener
{
  static Logger logger = LogManager.getLogger(MyRowsSetListener.class.getName());
  
  private MyCytoPanel mainApp;
  
  public SelectionRedirector(MyCytoPanel mainApp)
  {
    this.mainApp = mainApp;
  }

  @Override
  public void handleEvent(SetCurrentNetworkViewEvent event)
  {
    logger.debug("active network:" + event.getNetworkView().getSUID());
    try
    {
      CyNetworkView view = event.getNetworkView();
      Class clazz = view.getClass();
      Method method = clazz.getDeclaredMethod("getCanvas");
      Component comp = (Component)method.invoke(view);
      //InnerCanvas canvas  = ((DGraphView)event.getNetworkView()).getCanvas(); 
      for(MouseListener item : comp.getMouseListeners())
        comp.removeMouseListener(item);
      MouseEventHandler handler = new MouseEventHandler(comp,view,mainApp);
      comp.addMouseListener(handler);
      comp.addKeyListener(handler);
      this.mainApp.setCurrentSelectMem(this.mainApp.getSelectedNodeSharedNames(view.getModel()));
    }
    catch(Throwable th)
    {
      logger.debug(th);
      th.printStackTrace();
    }   
  }  
}
