#!/bin/bash

#install java 8
#sudo add-apt-repository ppa:webupd8team/java
#sudo apt-get update
#sudo apt-get install oracle-java8-installer
#sudo update-java-alternatives -s java-8-oracle

#install bioconductor
#source("https://bioconductor.org/biocLite.R")
#biocLite(c("annotate"))

#tools need for Meme2Fimo
sudo apt-get install npm nodejs-legacy
sudo npm install -g inliner
