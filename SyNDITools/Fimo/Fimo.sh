#!/bin/bash 

echo "Meme.sh1"

#directory change source from http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

echo "Meme.sh2"

genelist=$1 
genomeAnnotation=$2 
result=$3
inputMemeDataSetId=$4
galaxyUrl=$5
apiKey=$6

echo "Meme.sh3"
    
mkdir res    

echo "Meme.sh4"
    
upstream=./res/upstream.fasta
upstreamBackground=./res/upstream.bg
genomeSeq=./res/genome.fasta
genomeBackground=./res/genome.bg
# python $DIR/getupstream.py $genomeAnnotation $upstream 1000
# python $DIR/getfasta.py $genomeAnnotation $genomeSeq 
# python $DIR/MotifFind.py $upstream $genelist ./res/in.fasta
echo "Meme.sh5"
python $DIR/../Meme2Fimo/getupstream.py $genomeAnnotation $upstream 1000
python $DIR/../Meme2Fimo/getfasta.py $genomeAnnotation $genomeSeq 
python $DIR/../Meme2Fimo/MotifFind.py $upstream $genelist ./res/in.fasta

echo "Meme.sh6"

# $DIR/meme/bin/fasta-get-markov -m 3 < $upstream > $upstreamBackground 2> /dev/null
# $DIR/meme/bin/fasta-get-markov -m 3 < $genomeSeq > $genomeBackground 2> /dev/null
# $DIR/meme/bin/meme.bin ./res/in.fasta -oc ./res/ -bfile $upstreamBackground -dna -revcomp -nmotifs 5 -mod zoops -evt 1000 2>&1

# We now try to work without inputMemeName

python $DIR/MemeDataSetIdToNameConvert.py $inputMemeDataSetId inputMemeName $galaxyUrl $apiKey
# echo "inputMemeDataSetId: $(inputMemeDataSetId)"
# echo "$inputMemeDataSetId"
# echo "inputMemeName: $(inputMemeName)"
# echo "$inputMemeName"

# Temprorarily hard code inputMemeName
# inputMemeName="Hard code MEME name"
echo "Meme.sh7"
# $DIR/../Meme2Fimo/meme/bin/fimo --bgfile $genomeBackground -oc ./res/fimo $inputMemeDataSetId $genomeSeq 2>&1
$DIR/../Meme2Fimo/meme/bin/fimo --bgfile $genomeBackground -oc ./res/fimo $inputMemeName $genomeSeq 2>&1
# $DIR/../Meme2Fimo/meme/bin/fimo --bgfile $genomeBackground -oc ./res/fimo inputMemeName $genomeSeq 2>&1
echo "Meme.sh8"

# python $DIR/GetMotifMatrix.py ./res/meme.html ./res/motifs.txt 
python $DIR/../Meme2Fimo/GetFimoGenelist.py $genomeAnnotation ./res/fimo 1
python $DIR/../Meme2Fimo/GetFimoGenelist.py $genomeAnnotation ./res/fimo 2
python $DIR/../Meme2Fimo/GetFimoGenelist.py $genomeAnnotation ./res/fimo 3
python $DIR/../Meme2Fimo/GetFimoGenelist.py $genomeAnnotation ./res/fimo 4
python $DIR/../Meme2Fimo/GetFimoGenelist.py $genomeAnnotation ./res/fimo 5
echo "Meme.sh9"
python $DIR/../Meme2Fimo/Merge.py ./res ./res/combined.html
inliner -n ./res/combined.html > $result 2> /dev/null
echo "Meme.sh10"
