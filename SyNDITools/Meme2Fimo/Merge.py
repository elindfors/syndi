import re
import sys

dir = sys.argv[1]
memeHtml = open(dir + "/meme.html")
outfile = open(sys.argv[2],"w")

motifs = file(dir + "/motifs.txt")
motiflines = motifs.readlines()

for i in xrange(len(motiflines) / 7):
	for line in memeHtml:
		if("Time " in line):
			outfile.write("<h4>Gene list and motif score <a href=\"#diagrams_doc\" class=\"help\"><div class=\"help\"></div></a></h4>")
			outfile.write("<div class=\"pad\"><p><pre>" + "\n".join(motiflines[i*7:i*7+6]) + "</pre></p></div>")
			outfile.write("<h4>Fimo best hit list <a href=\"#diagrams_doc\" class=\"help\"><div class=\"help\"></div></a></h4>")
			outfile.write("<div class=\"pad\"><p>")
			filein = open(dir + "/fimo/goodlist" + str(i + 1) + ".html")
			outfile.write(filein.read())
			filein.close()
			outfile.write("</p></div>")
			break
		outfile.write(line)		
		
for line in memeHtml:
	outfile.write(line)
	
