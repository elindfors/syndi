'''
Created on Nov 17, 2011

@author: jesse
'''
import re
import sys

input = sys.argv

#infile = open("../meme.html")
#outfile = open("../motifout.txt","w")
infile = open(input[1])
outfile = open(input[2],"w")

letters = ["a","c","g","t"]
num = 0
count = 1
tmp = ""
while True:
    line = infile.readline()
    if line == "":
        break
    if line.startswith("<input type=\"hidden\" id=\"blocks"):
        infile.readline() #skip one
        line = infile.readline()
        while True:
            if(line.startswith("//")):
                break
            geneName = re.match("[^\|]*\|([^\|]*)\|.*",line).group(1)
            outfile.write(";" + geneName)
            line = infile.readline()
        outfile.write("\n")
        outfile.write(tmp)
        tmp = ""
            
    if line.startswith("<input type=\"hidden\" id=\"pspm"):
        header = infile.readline()
        num = int(re.match("letter-probability\smatrix\:\salength\=\s\d*\sw\=\s\d*\snsites\=\s(\d*)\s.*",header).group(1))
        vals = []
        while True:
            line = infile.readline()
            if line.startswith("\">") or line.startswith("\n"):
                break
            items = line.split(' ')
            vals.append([int(float(items[0])*num),int(float(items[1])*num),int(float(items[2])*num),int(float(items[3])*num)])
        tmp += ";motif " + str(count) + " " + header

        count = count + 1
        for i in xrange(0,4):
            tmp += letters[i] + " |"
            for j in xrange(0,len(vals)):
                tmp += " " + str(vals[j][i])
            tmp += "\n"
        tmp += "\n"
            
outfile.close    
print("done")    
