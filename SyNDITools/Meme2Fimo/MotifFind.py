import re
import sys
from Bio import SeqIO

source = SeqIO.parse(open(sys.argv[1]), "fasta")
geneListIn = open(sys.argv[2])
resFile = sys.argv[3]

input = [] 
for item in geneListIn:
    input.append(item.strip().lower())
result = []

geneListIn.close()

for req in source:
    match = re.match("[^\s]*\s(\w*);\supstream\sfrom\s(\-?\d*)\sto\s(\-?\d*);.*",req.description)
    if match.group(1).lower() in input and int(match.group(2)) < -8:
        result.append(req)
        
SeqIO.write(result, resFile, "fasta")
        
