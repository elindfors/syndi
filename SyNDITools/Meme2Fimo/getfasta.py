import sys
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import generic_protein

infile = sys.argv[1]
outfile = sys.argv[2]

source = SeqIO.read(open(infile), "genbank")

seq = str(source.seq)

out = open(outfile,"w")
out.write(">" + source.annotations["accessions"][0] + "." + str(source.annotations["sequence_version"]) + "\t" + source.annotations["source"] + "\n")
i = 0
while i < len(seq):
   out.write(seq[i:min(i + 80,len(seq))] + "\n")
   i += 80 
out.close();
        
