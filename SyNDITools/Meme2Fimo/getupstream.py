import sys
from Bio import SeqIO
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord
from Bio.Alphabet import generic_protein

infile = sys.argv[1]
outfile = sys.argv[2]
upstreamsize = int(sys.argv[3])

source = SeqIO.read(open(infile), "genbank")

result = []
boundaries = []
#boundaries.append(len(source.seq) + 1)
genelocs = []
seq = str(source.seq)

for feature in source.features:
    if feature.type == "CDS":
 	boundaries.append(feature.location.start.position)
	boundaries.append(feature.location.end.position)  
        genelocs.append([feature.location.start.position,feature.location.end.position])

boundaries.sort()
boundaries.append(boundaries[len(boundaries) - 1] - len(source.seq))
boundaries.append(boundaries[0] + len(source.seq))
boundaries.sort()

def getUpstreamRange(pos,strand,maxlen):
    #check if pos is inside a gene, this max the boundary checking more simple
    for geneloc in genelocs:
	if pos >= geneloc[0] and pos < geneloc[1]:
	    return [0,0]
    i = 0
    #simple boundary check, begin and end boundaries are in one array
    for bound in boundaries:
        if bound > pos:
            break
        i += 1
    if strand == 1:
 	return [max(boundaries[i - 1],pos - maxlen),pos + 1]
    else:
        return [pos,min(boundaries[i],pos + maxlen)]

def getSeq(start,end):
   if start < 0:
       return seq[len(seq) + start - 1:len(seq) - 1] + seq[0:end]
   if end > len(seq) - 1:
       return seq[start:len(seq) - 1] + seq[0:end - len(seq) + 1]
   return seq[start:end]
    
out = open(outfile,"w")
accesNum = source.annotations["accessions"][0] + "." + str(source.annotations["sequence_version"])
for feature in source.features: 
    if feature.type == "CDS":
        seqRange = []
	strandName = "D"
	startPos = 0
	if feature.location.strand == 1:
            startPos = feature.location.start.position - 1
	    seqRange = getUpstreamRange(startPos,1,upstreamsize)
	else:
	    strandName = "R"
            startPos = feature.location.end.position
            seqRange = getUpstreamRange(startPos,-1,upstreamsize)
	size = seqRange[0] - seqRange[1]

	geneName = ""
	locosTag =  feature.qualifiers["locus_tag"][0]
	if "gene" in feature.qualifiers:
	    geneName = feature.qualifiers["gene"][0]
	else:
	    geneName = locosTag
	out.write(">" + str(feature.qualifiers['protein_id'][0]) + "|" + geneName + "|" + source.annotations["source"].replace(' ','_') + 
              "|upstream|" + str(size) + "|-1|" + accesNum + "|" + strandName + "|" + str(seqRange[0] + 1) + "|" + str(seqRange[1]) +
              "\t" + locosTag + "; upstream from " + str(size) + " to -1; size: " + str(-size) + "; feature type:cds; location:" +
              source.annotations["source"].replace(' ','_') + ":" + accesNum + ":" + str(seqRange[0] + 1) + "-" + str(seqRange[1]) + ":" + strandName + ";\n")
	if strandName == "D":
	    out.write(getSeq(seqRange[0],seqRange[1]) + "\n")
        else:
            out.write(str(Seq(getSeq(seqRange[0],seqRange[1])).reverse_complement()) + "\n")

out.close();
        
