#!/bin/bash 

#directory change source from http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do # resolve $SOURCE until the file is no longer a symlink
  DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE" # if $SOURCE was a relative symlink, we need to resolve it relative to the path where the symlink file was located
done
DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

genelist=$1 
genomeAnnotation=$2 
result=$3 
    
mkdir res    
    
upstream=./res/upstream.fasta
upstreamBackground=./res/upstream.bg
genomeSeq=./res/genome.fasta
genomeBackground=./res/genome.bg
python $DIR/getupstream.py $genomeAnnotation $upstream 1000
python $DIR/getfasta.py $genomeAnnotation $genomeSeq 
python $DIR/MotifFind.py $upstream $genelist ./res/in.fasta

$DIR/meme/bin/fasta-get-markov -m 3 < $upstream > $upstreamBackground 2> /dev/null
$DIR/meme/bin/fasta-get-markov -m 3 < $genomeSeq > $genomeBackground 2> /dev/null
$DIR/meme/bin/meme.bin ./res/in.fasta -oc ./res/ -bfile $upstreamBackground -dna -revcomp -nmotifs 5 -mod zoops -evt 1000 2>&1
$DIR/meme/bin/fimo --bgfile $genomeBackground -oc ./res/fimo ./res/meme.xml $genomeSeq 2>&1
      
python $DIR/GetMotifMatrix.py ./res/meme.html ./res/motifs.txt 
python $DIR/GetFimoGenelist.py $genomeAnnotation ./res/fimo 1
python $DIR/GetFimoGenelist.py $genomeAnnotation ./res/fimo 2
python $DIR/GetFimoGenelist.py $genomeAnnotation ./res/fimo 3
python $DIR/GetFimoGenelist.py $genomeAnnotation ./res/fimo 4
python $DIR/GetFimoGenelist.py $genomeAnnotation ./res/fimo 5

python $DIR/Merge.py ./res ./res/combined.html
inliner -n ./res/combined.html > $result 2> /dev/null
