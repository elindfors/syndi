'''
Created on Jan 7, 2012

@author: jesse
'''
from Bio import SeqIO
from Bio import Seq
import sys

gbkfile = sys.argv[1]
dir = sys.argv[2]
motif = int(sys.argv[3])
#gbkfile = "/home/jesse/code/workdir/fimo.gbk"
#dir = "/home/jesse/programs/diva/workdir/motifresult/47/fimo"

if(not dir.endswith("/")):
    dir = dir + "/"

class Region():
    def __init__(self):
        self.start = -1
        self.end = -1
        self.gene = "none"
        self.leftgene = "none"
        self.rigthgene = "none"
        self.inside = 1
        
    def toStr(self):
        return "" + str(self.start) + ":" + str(self.end) + " " + str(self.inside)

class MatchDetail():
    def __init__(self):
        self.count = 1
	self.pvalue = 0
        self.qvalue = 0
        self.hitnum = -1
        self.countergene = "none"
        self.sequence = "<empty>"
        self.relpos = 0
        
def writegood(gene,countergene,sequence,pvalue,qvalue,relpos):
    if(goodcount.has_key(gene)):
        goodcount[gene].count = goodcount[gene].count + 1
    else:
        if(gene in motifhit or (not countergene in motifhit)):
            printlist.append(gene)
            #goodlist.write(gene + ";")
        goodcount[gene] = MatchDetail()
        goodcount[gene].pvalue = pvalue
        goodcount[gene].qvalue = qvalue
        goodcount[gene].countergene = countergene
        goodcount[gene].sequence = sequence
        goodcount[gene].relpos = relpos
        if(gene in motifhit):
            goodcount[gene].hitnum = motifhit.index(gene)
        goodlistitems.append(gene)

source = SeqIO.read(open(gbkfile), "genbank")

source.features = filter(lambda req: req.type == "gene",source.features)
source.features = sorted(source.features, key=lambda feature: feature.location.start)

regulatory = []
ingene = []

alias2LocusMap = {}

#first setup our regions so it makes the searching more easy
#there are regions of genes and there are intergenic regions
prev = 0
previtem = source.features[0]
for req in source.features[1:-1]:
    #print(req.location.start.__dict__)
    if(req.qualifiers.has_key("gene")):
        if(alias2LocusMap.has_key(req.qualifiers["gene"][0])):
            print("double alias found" + req.qualifiers["gene"][0])
        alias2LocusMap[req.qualifiers["gene"][0]] = req.qualifiers["locus_tag"][0]
    region = Region()
    region.start = previtem.location.start.position
    region.end = previtem.location.end.position
    region.gene = previtem.qualifiers["locus_tag"][0].upper()
    ingene.append(region)
    if(req.location.start.position > prev):
        region = Region()
        region.start = previtem.location.end.position
        region.end = req.location.start.position
        #region.gene = previtem.qualifiers["locus_tag"][0]
        region.inside = 0
        if(previtem.qualifiers["locus_tag"][0].endswith("c")):
            region.leftgene = previtem.qualifiers["locus_tag"][0].upper()
        if(not req.qualifiers["locus_tag"][0].endswith("c")):
            region.rigthgene = req.qualifiers["locus_tag"][0].upper()    
        regulatory.append(region)       
    prev = req.location.end.position
    previtem = req
#add the last one
region = Region()
region.start = previtem.location.start.position
region.end = previtem.location.end.position
region.gene = previtem.qualifiers["locus_tag"][0].upper()
ingene.append(region)
#    print(req.qualifiers["locus_tag"][0].endswith("c"))
#    print(req.location)

#resout = file(dir + "resout.txt","w")
fimoin = file(dir + "fimo.txt")
motifs = file(dir + "../motifs.txt")
fimoin.readline()

motiflines = motifs.readlines()
if len(motiflines) > (motif - 1) * 7:
	motifline = motiflines[(motif - 1) * 7]
	#print(motifline)
	motifhit = motifline.strip()[1:len(motifline)].split(";")
	#print("checkking: " + str(motifhit))
	
	goodlist = file(dir + "goodlist" + str(motif) + ".html","w")
        goodlisttxt = file(dir + "goodlist" + str(motif) + ".txt","w")
	
	for i in xrange(0,len(motifhit)):
	    if(alias2LocusMap.has_key(motifhit[i])):
	        motifhit[i] = alias2LocusMap[motifhit[i]]
	    motifhit[i] = motifhit[i].upper()
	  
	print(motifhit)
	goodcount = {}
	goodlistitems = []
	printlist = []
	
	for line in fimoin:
	    fields = line.strip().split("\t")
	    if(int(fields[0]) != motif):
	        continue
            pvalue = float(fields[6])
	    qvalue = float(fields[7])
	    pos1 = int(fields[2])
	    pos2 = int(fields[3])
	    found = 0
	    #first check for intergenic regions
	    distance = 0
	    goodhit = 0
	    for region in regulatory:
	        if((pos1 > region.start and pos1 < region.end) or (pos2 > region.start and pos2 < region.end)):
	            found = region
	            goodhit = 1
	            break
	    #not found check which gene it is
	    if(found == 0):
	        for region in ingene:
	            if((pos1 > region.start and pos1 < region.end) or (pos2 > region.start and pos2 < region.end)):
	                distance = min(abs(pos1 - region.start),abs(pos1 - region.end),abs(pos2 - region.start),abs(pos2 - region.end))
	                found = region
	                break
	    motiflength = abs(pos1 - pos2)
	    if(goodhit == 1):
	        if(found.rigthgene != "" or found.leftgene):
	            #resout.write(line.strip() + "\tgood\t" + found.leftgene + "\t" + found.rigthgene + "\n")
	            sequence = fields[8]
	            if(pos1 > pos2):
	                sequence = str(Seq.Seq(sequence).reverse_complement())
	            if(found.leftgene != "none"):
	                writegood(found.leftgene,found.rigthgene,sequence,pvalue,qvalue,region.start - pos1 - motiflength)
	            if(found.rigthgene != "none"):
	                writegood(found.rigthgene,found.leftgene,sequence,pvalue,qvalue,pos2 - region.end - motiflength - 1)
	        #else:
	        #    resout.write(line.strip() + "\t->|<-\t" + found.leftgene + "\t" + found.rigthgene + "\n")
	    #elif(found != 0):
	    #    resout.write(line.strip() + "\t" + str(distance) + "\t" + found.gene + "\t\n")
	    #else:
	    #    print("nothing found " + str(pos1) + ":" + str(pos2))
	     
	#resout.close()
	
        #a downstream gene identifier, a sequence associated to the occurrence, an index of the downstream gene within the initial MEME result, an index of the upstream gene within the initial MEME result, a p-value, a q-value, a hit count and relative position to the downstream gene.
	goodlist.write("<table>")
        goodlist.write("\n<tr><td><b>DownStream <br>gene ID </b></td><td><b>Index downstream<br>gene</b></td><td><b>Index upstream<br>gene</b></td><td><b>Upstream <br>gene ID</b></td><td><b>Q-Value</b></td><td><b>P-Value</b></td><td><b>Hit count</b></td><td><b>Relative position<br>of downstream gene</b></td><td><b>Sequence <br>of occurrence</b></td></tr>\n\n")
        goodlisttxt.write("\nDownStream gene ID\tIndex downstream gene\tIndex upstream gene\tUpstream gene ID\tQ-Value\tP-Value\tHit count\tRelative position of downstream gene\tSequence of occurrence\n\n")
	temp = ""
	for key in goodlistitems:
	    counterindex = -1
	    item = goodcount[key]
	    if(goodcount.has_key(item.countergene)):
	        counterindex = goodcount[item.countergene].hitnum
	    goodlist.write("<tr><td>" + key + "</td><td>" + str(item.hitnum) + "</td><td>" + str(counterindex) + "</td><td>" + str(item.countergene) + "</td><td>" + str(item.qvalue) + "</td><td>" + str(item.pvalue) + "</td><td>" + str(item.count) + "</td><td>" + str(item.relpos) + "</td><td>" + str(item.sequence) + "</td></tr>")
	    goodlisttxt.write(key + "\t" + str(item.hitnum) + "\t" + str(counterindex) + "\t" + str(item.countergene) + "\t" + str(item.qvalue) + "\t" + str(item.pvalue) + "\t" + str(item.count) + "\t" + str(item.relpos) + "\t" + str(item.sequence) + "\n")
	    
	goodlist.write("\n</table>");
	goodlist.write("\n\n<p><b>Gene short list</b>\n")
        goodlisttxt.write("\n\nGene short list\n")
        goodlist.write("<pre>")
	for gene in printlist:
	    temp = temp + gene + ";"
	    if(len(temp) > 150):
	    	goodlist.write(temp + "\n")
                goodlisttxt.write(temp + "\n")
	    	temp = ""    
        goodlist.write("</pre></p>")	
	goodlist.close()    
        goodlisttxt.close()
            
    
#fimoin.close()
